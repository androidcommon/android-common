package com.library.android_common.component.view.dialog;

/**
 * Created by Raymond Zorro on 27/04/2018.
 * Login alert Dialog
 */
// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class LogInDialog {
//
//    static public void alertLoginRequest(final Activity activity) { // 登入頁面若是不登入就繼續，警告使用者
//        final IOSAlertDialog caDialog = new IOSAlertDialog(activity, MutiMsg.LOGIN_TITLE.msg(), MutiMsg.LOGIN_ALERT.msg());
//        caDialog.show(new IOSAlertDialog.OnClickEvent() {
//            @Override
//            public void confirm() {
//                caDialog.dismiss();
//            }
//
//            @Override
//            public void cancel() {
//                caDialog.dismiss();
//            }
//        });
//    }
//
//    static public void moveToLogin(final Activity activity) { // 我的列表提醒使用者前往登入
//        final IOSAlertDialog caDialog = new IOSAlertDialog(activity, MutiMsg.LOGIN_TITLE.msg(), MutiMsg.LOGIN_REQUEST.msg());
//        caDialog.show(new IOSAlertDialog.OnClickEvent() {
//            @Override
//            public void confirm() {
//                caDialog.dismiss();
//            }
//
//            @Override
//            public void cancel() {
//                caDialog.dismiss();
//            }
//        });
//    }
//}
