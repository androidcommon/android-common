package com.library.android_common.component.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;

import com.library.android_common.component.TimeCounter;
import com.library.android_common.util.ThreadUtil;


/**
 * Created by Ray on 18/05/2018.
 * 透過背景 thread 呼叫 Progress dialog
 * delay 的用意是讓 thread  執行前，先讓 user 看到 dialog show
 */

public class PTDialog { // PT means Progress Task

    private static OnDialogShowListener sm_listener;
    private static OnAsyncTaskListener sm_syncTaskListener;
    private static OnAsyncMsgListener sm_asyncMsgListener;

    private static ProgressDialog sm_customDialog;
    private static int m_delay = 1; // default 1 second

    //-----------------------------------------------------------
    // Main Method
    //-----------------------------------------------------------
    static public void show(Activity a, String msg) { // just show dialog
        show(a, msg, new OnDialogShowListener() {
            @Override
            public void onDialogShow() {
                // do nothing
            }
        });
    }

    static public void show(Activity a, String msg, OnDialogShowListener listener) {
        sm_listener = listener;
        init(a, msg);
    }

    static public void show(Activity a, int delay_second, String msg, OnDialogShowListener listener) {
        sm_listener = listener;
        m_delay = delay_second;
        init(a, msg);
    }

    static public void show(Activity a, String msg, OnAsyncTaskListener listener) { // TODO 暫時先不使用
        sm_syncTaskListener = listener;
        initAsync(a, msg);
    }

    //-----------------------------------------------------------
    // Dialog Init
    //-----------------------------------------------------------
    static private void initAsync(final Activity currentActivity, String msg) { // TODO 暫時先不使用
        sm_customDialog = new ProgressDialog(currentActivity);
        sm_customDialog.setMessage(msg);
        sm_customDialog.show();

        //-----------------------------------------
        // 此處延遲是為了讓 user 看到 Progress dialog
        TimeCounter.countDown(m_delay, new TimeCounter.onTimeUpListener() {
            @Override
            public void onFinish() {
                ThreadUtil.createThread(new ThreadUtil.OnRunThreadFinish() {
                    @Override
                    public void run() {
                        sm_syncTaskListener.onAsyncDialogShow();
                    }

                    @Override
                    public void finish() {
                        if (currentActivity != null) { // run on ui thread
                            currentActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    sm_syncTaskListener.onFinish();
                                }
                            });
                        }
                    }
                });

            }
        });

    }

    static private void init(Activity a, String msg) {
        sm_customDialog = new ProgressDialog(a);
        sm_customDialog.setMessage(msg);
        sm_customDialog.show();

        //-----------------------------------------
        // 此處延遲是為了讓 user 看到 Progress dialog
        TimeCounter.countDown(m_delay, new TimeCounter.onTimeUpListener() {
            @Override
            public void onFinish() {
                sm_listener.onDialogShow();
            }
        });
    }

    //-----------------------------------------------------------
    // Public Method
    //-----------------------------------------------------------
    static public void dismiss() {
        if (sm_customDialog != null)
            sm_customDialog.dismiss();
    }

    static public void setMessage(String msg) {
        if (sm_customDialog != null)
            sm_customDialog.setMessage(msg);
    }

    static public void _setMessage(Activity activity, final String msg, OnAsyncMsgListener asyncMsgListener) { // TODO 暫時先不使用
        sm_asyncMsgListener = asyncMsgListener;
        //---------------------------------------
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setMessage(msg);
            }
        });

        //---------------------------------------
        ThreadUtil.createThread(new ThreadUtil.OnRunInNewThread() {
            @Override
            public void run() {
                sm_asyncMsgListener.onFinish(); // listener Async Thread
            }
        });
    }

    static public Dialog getInstance() {
        return sm_customDialog;
    }


    //-----------------------------------------------------------
    // interface
    //-----------------------------------------------------------
    public interface OnDialogShowListener {
        void onDialogShow();
    }

    public interface OnAsyncTaskListener { // 用以導回 Main Thread // maybe 不是一個完美的做法 // TODO 暫時先不使用
        void onAsyncDialogShow(); // after dialog show and Asyn

        void onFinish(); // in ui thread
    }

    public interface OnAsyncMsgListener { // 當 setMsg 結束之後異步 listener // TODO 暫時先不使用
        void onFinish();
    }
}
