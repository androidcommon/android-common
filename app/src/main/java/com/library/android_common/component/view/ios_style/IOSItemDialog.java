package com.library.android_common.component.view.ios_style;

/**
 * Create By Ray on 2018/6/29
 * 1. 可從外部添加 Button 數
 * 2. 並透過 interface 將 click listener 帶出去
 */
// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class IOSItemDialog {
//
//    private final int TEXTVIEW_TEXT_SIZE = 12;
//    private final int TEXTVIEW_HEIGHT = 150;
//    private final int BUTTON_HEIGHT = 180;
//    private final int BUTTON_TEXT_SIZE = 16;
//    private final int DIVIDER_HEIGHT = 1;
//
//    private final String DIVIDER_COLOR_CODE = "#cdcdcd";
//
//    private IOSItemDialog m_self;
//    private Dialog m_dialog;
//    private OnItemClicklistener m_itemClicklistener;
//
//    //----------------------------------------------------------------------
//    // Construct
//    //----------------------------------------------------------------------
//    public IOSItemDialog(Context ctx, ArrayList<String> btnNames) {
//        init(ctx, StrUtil.EMPTY, btnNames);
//    }
//
//    public IOSItemDialog(Context ctx, String title, ArrayList<String> btnNames) {
//        init(ctx, title, btnNames);
//    }
//
//    private void init(final Context ctx, String title, final ArrayList<String> btnNames) {
//        m_self = this;
//        //------------------------------------------------------
//        // declare
//        m_dialog = new Dialog(ctx);
//        Button[] btns = new Button[btnNames.size()];
//        final IOSLinearLayout iosllview = new IOSLinearLayout(ctx);
//
//        //------------------------------------------------------
//        // set data
//        iosllview.setOrientation(LinearLayout.VERTICAL);
//        iosllview.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//        iosllview.setGravity(View.TEXT_ALIGNMENT_CENTER);
//
//        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        m_dialog.addContentView(iosllview, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//        //------------------------------------------------------
//        // set title
//        if (!Opt.of(title).getOr(StrUtil.EMPTY).isEmpty()) {
//            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, TEXTVIEW_HEIGHT);
//            lp.gravity = Gravity.CENTER;
//
//            TextView txtTitle = new TextView(ctx);
//            txtTitle.setLayoutParams(lp);
//            txtTitle.setGravity(Gravity.CENTER);
//            txtTitle.setText(title);
//            txtTitle.setTextSize(TEXTVIEW_TEXT_SIZE);
//
//            iosllview.addView(txtTitle);
//            iosllview.addView(divider(ctx));
//        }
//
//        //------------------------------------------------------
//        // add item
//        Lst.of(btns).forEach(new Lst.IConsumer<Button>() {
//            @Override
//            public void runEach(int i, Button btn) {
//
//                btn = new Button(ctx);
//                btn.setBackgroundColor(Color.TRANSPARENT);
//                btn.setLayoutParams(itemParams());
//                btn.setText(btnNames.get(i));
//                btn.setTextSize(BUTTON_TEXT_SIZE);
//                btn.setTextColor(Color.parseColor(IOSTheme.Color.TEXT_BLUE.code()));
//                btn.setId(i);
//
//                iosllview.addView(btn);
//                if (i != btnNames.size() - 1)
//                    iosllview.addView(divider(ctx)); // divider
//
//                //-----------------------------------------------------
//                // listener
//                btn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) { m_itemClicklistener.onItemClick(v.getId(), v);
//                    }
//                });
//            }
//        });
//    }
//
//    //----------------------------------------------------------------------
//    // Private Preference
//    //----------------------------------------------------------------------
//    private LinearLayout.LayoutParams itemParams() {
//        return new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, BUTTON_HEIGHT);
//    }
//
//    private View divider(Context ctx) {
//        View v = new View(ctx);
//        v.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DIVIDER_HEIGHT));
//        v.setBackgroundColor(Color.parseColor(DIVIDER_COLOR_CODE));
//        return v;
//    }
//
//    //----------------------------------------------------------------------
//    // Public Method
//    //----------------------------------------------------------------------
//    public void show(OnItemClicklistener itemClicklistener) {
//        m_itemClicklistener = itemClicklistener;
//        m_dialog.show();
//    }
//
//    public void dismiss() {
//        m_dialog.dismiss();
//    }
//
//    public Dialog getInstance() {
//        return m_dialog;
//    }
//
//    public IOSItemDialog setCancelable(boolean b) {
//        m_dialog.setCancelable(b);
//        return m_self;
//    }
//
//    //----------------------------------------------------------------------
//    // Interface
//    //----------------------------------------------------------------------
//    public interface OnItemClicklistener {
//        void onItemClick(int pos, View view);
//    }
//
//}
