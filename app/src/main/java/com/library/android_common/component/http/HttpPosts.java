package com.library.android_common.component.http;

import android.os.AsyncTask;

import com.library.android_common.component.common.Pair;
import com.library.android_common.util.JSONUtil;
import com.library.android_common.util.StrUtil;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by Raymond Zorro on 08/05/2018.
 * To use AsyncTask redirect call api thread to UI main thread
 */

public class HttpPosts extends AsyncTask<Pair<List<NameValuePair>, String>, Void, Pair<String, JSONObject>> {

    @Override
    protected void onPostExecute(Pair<String, JSONObject> stringJSONObjectPair) {
        super.onPostExecute(stringJSONObjectPair);
    }

    @Override
    protected Pair<String, JSONObject> doInBackground(Pair<List<NameValuePair>, String>... pairs) {

        String sUrl = pairs[0].v();
        String sRlt = StrUtil.EMPTY;
        List<NameValuePair> lst = pairs[0].k();

        try {

            HttpPost httpPost = new HttpPost(sUrl);
            httpPost.setEntity(new UrlEncodedFormEntity(lst, HTTP.UTF_8));

            HttpResponse res = new DefaultHttpClient().execute(httpPost);
            sRlt = EntityUtils.toString(res.getEntity(), HTTP.UTF_8);

        }catch (IOException e){
            e.getMessage();
        }

        return Pair.of(sRlt, JSONUtil.toJSObj(sRlt));
    }
}
