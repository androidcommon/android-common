package com.library.android_common.component.view.dialog;

/**
 * Create By Ray on 2018/6/22
 * 選擇語言時跳出的 dialog
 */
// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class SelectLanguageDialog {
//
//    private IOSItemDialog m_dialog;
//    private OnChangeLanguageListener m_listener;
//    private Pairs<String, String> m_options = Pairs.of(
//            MutiMsg.LANGUAGE_EG.msg(), MutiMsg.ENGLISH)
//            .add(MutiMsg.LANGUAGE_CH.msg(), MutiMsg.CHINESE)
//            .add(MutiMsg.LANGUAGE_SC.msg(), MutiMsg.SIMCHINESE)
//            .add(MutiMsg.LANGUAGE_JP.msg(), MutiMsg.JAPANESE);
//
//    //--------------------------------------------------
//    // Construct
//    //--------------------------------------------------
//    public SelectLanguageDialog(Activity activity) {
//        m_dialog = new IOSItemDialog(activity, m_options.toKList());
//    }
//
//    //--------------------------------------------------
//    // public Method
//    //--------------------------------------------------
//    public void show(OnChangeLanguageListener listener) {
//        this.m_listener = listener;
//        m_dialog.setCancelable(false)
//                .show(new IOSItemDialog.OnItemClicklistener() {
//                    @Override
//                    public void onItemClick(int pos, View v) {
//                        SpCurrentLanguage.put(m_options.toVList().get(pos));
//                        m_dialog.dismiss();
//                        m_listener.finish();
//                    }
//                });
//    }
//
//    public void dismiss() {
//        m_dialog.dismiss();
//    }
//
//    //--------------------------------------------------
//    // Listener
//    //--------------------------------------------------
//    public interface OnChangeLanguageListener {
//        void finish();
//    }
//
//}
