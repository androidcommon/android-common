package com.library.android_common.component.view;

/**
 * Created by Raymond Zorro on 24/04/2018.
 */
// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class SexualPicker implements View.OnClickListener {
//
//    private int m_currentID = -1;
//
//    private Activity m_activity;
//    private Dialog m_dialog;
//    private Button m_btnMale, m_btnFemale, m_btnThird;
//    private PickListener m_pickListener;
//
//    //----------------------------------------------------
//    // construct
//    //----------------------------------------------------
//
//    public SexualPicker(Activity activity) {
//        this.m_activity = activity;
//
//        //-----------------------------------------------------------
//        // declare
//        m_dialog = new Dialog(activity);
//        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        m_dialog.setContentView(R.layout.comp_sexual_dialog);
//
//        ((TextView) m_dialog.findViewById(R.id.dialog_my_formula_add_title)).setText(MutiMsg.GENDER.msg());
//        m_btnMale = (Button) m_dialog.findViewById(R.id.comp_sexuale_male);
//        m_btnFemale = (Button) m_dialog.findViewById(R.id.comp_sexuale_female);
//        m_btnThird = (Button) m_dialog.findViewById(R.id.comp_sexuale_third_gender);
//
//        //-----------------------------------------------------------
//        // set data、listener
//        m_btnMale.setOnClickListener(this);
//        m_btnFemale.setOnClickListener(this);
//        m_btnThird.setOnClickListener(this);
//
//        m_btnMale.setText(MutiMsg.MALE.msg());
//        m_btnFemale.setText(MutiMsg.FEMALE.msg());
//        m_btnThird.setText(MutiMsg.THIRD_GENDER.msg());
//
//        m_btnFemale.setTextColor(Color.parseColor(IOSTheme.Color.TEXT_BLUE.code()));
//        m_btnMale.setTextColor(Color.parseColor(IOSTheme.Color.TEXT_BLUE.code()));
//        m_btnThird.setTextColor(Color.parseColor(IOSTheme.Color.TEXT_BLUE.code()));
//
//    }
//
//    //----------------------------------------------------
//    // public method
//    //----------------------------------------------------
//    public void showPicker(PickListener pickListener) {
//        this.m_pickListener = pickListener;
//        m_dialog.show();
//    }
//
//    public void dismiss() {
//        m_dialog.dismiss();
//    }
//
//    public int getSexualID() {
//        return m_currentID;
//    }
//
//    public static ArrayList<String> sexualOpts() { // sexual options
//        return Lst.of(MutiMsg.THIRD_GENDER.msg(), MutiMsg.MALE.msg(), MutiMsg.FEMALE.msg()).toList();
//    }
//
//    public static String getSexualTxt(int pos) {
//        return Lst.of(sexualOpts()).get(pos);
//    }
//
//    public static int getSexualID(String sSexual) {
//        return Lst.of(sexualOpts()).find(sSexual.trim());
//    }
//
//    private void chgBtnSytle(Button btn) {
//        btn.setBackgroundColor(Color.parseColor(ColorUtil.WHITE));
//        btn.setTextColor(m_activity.getResources().getColor(R.color.colorMainBackground));
//    }
//
//
//    //----------------------------------------------------
//    // implement method
//    //----------------------------------------------------
//    @Override
//    public void onClick(View view) {
//
//        switch (view.getId()) {
//            //------------------------------
//            case R.id.comp_sexuale_male:
//                m_pickListener.choice(MutiMsg.MALE.msg());
//                chgBtnSytle(m_btnMale);
//
//                m_dialog.dismiss();
//                break;
//
//            //------------------------------
//            case R.id.comp_sexuale_female:
//                m_pickListener.choice(MutiMsg.FEMALE.msg());
//                chgBtnSytle(m_btnFemale);
//
//                m_dialog.dismiss();
//                break;
//
//            //------------------------------
//            case R.id.comp_sexuale_third_gender:
//                m_pickListener.choice(MutiMsg.THIRD_GENDER.msg());
//                chgBtnSytle(m_btnThird);
//
//                m_dialog.dismiss();
//                break;
//
//            //------------------------------
//        }
//    }
//
//    //----------------------------------------------------
//    // listener
//    //----------------------------------------------------
//    public interface PickListener {
//        void choice(String sexual);
//    }
//
//}
