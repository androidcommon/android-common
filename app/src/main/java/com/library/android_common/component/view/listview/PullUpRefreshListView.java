package com.library.android_common.component.view.listview;

/**
 * Create By Ray on 2018/5/24
 * 當 ListView Pull Up Refresh
 * reference page : https://www.jianshu.com/p/95aae520f548
 * 上拉加載
 */
// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class PullUpRefreshListView extends ListView implements AbsListView.OnScrollListener {
//
//    //是否加载中或已加载所有数据
//    private boolean mIsLoadingOrComplete;
//    //是否所有条目都可见
//    private boolean mIsAllVisible;
//
//    private OnLoadMoreListener mOnLoadMoreListener;
//    private View mLoadMoreView;
//    private View mLoadCompleteView;
//
//    public PullUpRefreshListView(Context context) {
//        super(context);
//        init(context);
//    }
//
//    public PullUpRefreshListView(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        init(context);
//    }
//
//    public PullUpRefreshListView(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        init(context);
//    }
//
//
//    @Override
//    public void onScrollStateChanged(AbsListView view, int scrollState) {
//        //(最后一条可见item==最后一条item)&&(停止滑动)&&(!加载数据中)&&(!所有条目都可见)
//        if (view.getLastVisiblePosition() == getAdapter().getCount() - 1 && scrollState == SCROLL_STATE_IDLE && !mIsLoadingOrComplete && !mIsAllVisible) {
//            if (null != mOnLoadMoreListener) {
//                //加载更多(延时1.5秒,防止加载速度过快导致加载更多布局闪现)
//                mIsLoadingOrComplete = true;
//                postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mOnLoadMoreListener.loadMore();
//                    }
//                }, 1000);
//            }
//        }
//        if (getFooterViewsCount() == 0 && !mIsAllVisible) addFooterView(mLoadMoreView);
//    }
//
//    @Override
//    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//        mIsAllVisible = totalItemCount == visibleItemCount;
//    }
//
//
//    public void setLoadCompleted(final boolean allComplete) {
//        if (allComplete && getFooterViewsCount() != 0) {
//            removeFooterView(mLoadMoreView);
//            removeFooterView(mLoadCompleteView);
//            addFooterView(mLoadCompleteView);
//        } else {
//            mIsLoadingOrComplete = false;
//        }
//    }
//
//
//    //--------------------------------------------------------------------
//    //初始化
//    private void init(Context context) {
//        mLoadMoreView = LayoutInflater.from(context).inflate(R.layout.comp_dialog_load_more, null);
//        mLoadCompleteView = LayoutInflater.from(context).inflate(R.layout.comp_dialog_load_complete, null);
//        setOnScrollListener(this);
//    }
//
//    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
//        mOnLoadMoreListener = onLoadMoreListener;
//    }
//
//    //--------------------------------------------------------------------
//    // interface
//    //--------------------------------------------------------------------
//    public interface OnLoadMoreListener {
//        void loadMore();
//    }
//
//}
