package com.library.android_common.component.view;

import android.app.Activity;
import android.view.WindowManager;

/**
 * Created by Raymond Zorro on 06/05/2018.
 * 設定背景的 alpha 值
 */

public class AlphaBackground {

    //------------------------------------------------------
    // hlaf alpha
    static public void half(Activity activity) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 0.5f;
        activity.getWindow().setAttributes(lp);
    }

    //------------------------------------------------------
    // normal
    static public void normal(Activity activity) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = 1f;
        activity.getWindow().setAttributes(lp);
    }

    //------------------------------------------------------
    // custom
    static public void custom(Activity activity, float alpha) {
        WindowManager.LayoutParams lp = activity.getWindow().getAttributes();
        lp.alpha = alpha;
        activity.getWindow().setAttributes(lp);
    }

}
