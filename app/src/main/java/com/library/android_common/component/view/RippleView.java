package com.library.android_common.component.view;

/**
 * Created by Bruce on 2018/6/7.
 *
 * 水波（漣漪）特效
 */
// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class RippleView extends RelativeLayout {
//
//    // 水波顏色
//    private int rippleColor = getResources().getColor(R.color.colorCleanMainBackground);
//    // 最里面水波的圆
//    private float rippleStrokeWidth = 10;
//    // 水波半徑
//    private float rippleRadius = getResources().getDimension(R.dimen.rippleRadius);
//    // 畫筆
//    private Paint paint;
//    // 動畫標誌
//    private boolean animationRunning = false;
//    // 動畫集合
//    private AnimatorSet animatorSet;
//    // 自定view集合
//    private ArrayList<mRipplView> rippleViewList = new ArrayList<>();
//    // 每次動畫時間
//    private int rippleDurationTime = 4000;
//    // 水波數目
//    private int rippleAmount = 4;
//    // 每個水波依次出現的延遲
//    private int rippleDelay;
//
//
//    //----------------------------------------------------------
//    // Construct
//    //----------------------------------------------------------
//    public RippleView(Context context) {
//        super(context);
//        init();
//    }
//
//    public RippleView(Context context, AttributeSet attrs) {
//        super(context, attrs);
//        init();
//    }
//
//    public RippleView(Context context, AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        init();
//    }
//
//
//    private void init() {
//        rippleDelay = rippleDurationTime / rippleAmount;// 畫每個圓的時間間隔為一個圓的動畫時間除以總共出現圓的個數，達到每個圓出現的時間間隔一致
//
//        paint = new Paint();// 畫筆初始化
//        paint.setAntiAlias(true);
//        paint.setStyle(Paint.Style.STROKE);
//        paint.setColor(rippleColor);
//        paint.setStrokeWidth(25);
//
//        // layout set 水波置中
//        LayoutParams rippleParams = new LayoutParams((int) (1 * (rippleRadius + rippleStrokeWidth)), (int) (1 * (rippleRadius + rippleStrokeWidth)));
//        rippleParams.addRule(CENTER_IN_PARENT, TRUE);
//
//        animatorSet = new AnimatorSet();
//        animatorSet.setDuration(rippleDurationTime);
//        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
//        ArrayList<Animator> animatorList = new ArrayList<>();// 水波的物件
//        // 縮放、漸放
//        for (int i = 0; i < rippleAmount; i++) {
//
//            mRipplView rippleView = new mRipplView(getContext());
//            addView(rippleView, rippleParams);
//            rippleViewList.add(rippleView);
//
//            // 伸縮 X
//            float rippleScale = 6.0f;
//            final ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(rippleView, "ScaleX", 1.0f, rippleScale);
//            scaleXAnimator.setRepeatCount(ValueAnimator.INFINITE);
//            scaleXAnimator.setRepeatMode(ObjectAnimator.RESTART);
//            scaleXAnimator.setStartDelay(i * rippleDelay);
//            animatorList.add(scaleXAnimator);
//
//            // 伸縮 Y
//            final ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(rippleView, "ScaleY", 1.0f, rippleScale);
//            scaleYAnimator.setRepeatCount(ValueAnimator.INFINITE);
//            scaleYAnimator.setRepeatMode(ObjectAnimator.RESTART);//ObjectAnimator.RESTART
//            scaleYAnimator.setStartDelay(i * rippleDelay);
//            animatorList.add(scaleYAnimator);
//
//            // 透明度
//            final ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(rippleView, "Alpha", 1.0f, 0f);
//            alphaAnimator.setRepeatCount(ValueAnimator.INFINITE);
//            alphaAnimator.setRepeatMode(ObjectAnimator.RESTART);
//            alphaAnimator.setStartDelay(i * rippleDelay);
//            animatorList.add(alphaAnimator);
//
//        }
//        // 啟動動畫
//        animatorSet.playTogether(animatorList);
//    }
//
//
//    // 劃一個圓
//    private class mRipplView extends View {
//
//        mRipplView(Context context) {
//            super(context);
//            this.setVisibility(View.INVISIBLE);
//        }
//
//        //----------------------------------------------------------
//        // Super Method
//        //----------------------------------------------------------
//        // 参數說明：圓心的x坐標。圓心的y坐標。圓的半徑。繪圖筆。
//        @Override
//        protected void onDraw(Canvas canvas) {
//            // 圓的半徑，它的父layout寬或高（取最小）的一半
//            int radius = (Math.min(getWidth(), getHeight())) / 2;
//            canvas.drawCircle(radius, radius, radius - rippleStrokeWidth, paint);
//        }
//    }
//
//    //----------------------------------------------------------
//    // Public Method
//    //----------------------------------------------------------
//    // 啟動水波
//    public void startRippleAnimation() {
//        if (!isRippleAnimationRunning()) {
//            for (mRipplView rippleView : rippleViewList) {
//                rippleView.setVisibility(View.VISIBLE);
//            }
//            animatorSet.start();
//            animationRunning = true;
//        }
//    }
//
//    // 結束水波
//    public void stopRippleAnimation() {
//        for (mRipplView rippleView : rippleViewList)
//            rippleView.setVisibility(View.GONE);
//        animatorSet.end();
//        animationRunning = false;
//    }
//
//    //----------------------------------------------------------
//    // Private Preference
//    //----------------------------------------------------------
//    private boolean isRippleAnimationRunning() {
//        return animationRunning;
//    }
//
//}
