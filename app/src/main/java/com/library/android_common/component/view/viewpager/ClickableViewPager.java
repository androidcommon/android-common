package com.library.android_common.component.view.viewpager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Create By Ray on 2018/6/5
 * 每一個 item 都可以被點擊的 view pager
 * reference : https://stackoverflow.com/questions/16350987/viewpager-onitemclicklistener
 */
public class ClickableViewPager extends ViewPager {

    private OnItemClickListener mOnItemClickListener;

    //--------------------------------------------------------
    // Construct
    //--------------------------------------------------------
    public ClickableViewPager(Context context) {
        super(context);
        init();
    }

    public ClickableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    //---------------------------------------------------------
    // Private Preference
    //---------------------------------------------------------
    private void init() {

        final GestureDetector tapGestureDetector = new GestureDetector(getContext(), new TapGestureListener());
        setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tapGestureDetector.onTouchEvent(event);
                return false;
            }
        });
    }

    //---------------------------------------------------------
    // Interface
    //---------------------------------------------------------
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }


    //---------------------------------------------------------
    // Inner Class
    //---------------------------------------------------------
    private class TapGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(getCurrentItem());
            }
            return true;
        }
    }


}
