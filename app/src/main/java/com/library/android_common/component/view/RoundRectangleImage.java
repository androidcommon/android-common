package com.library.android_common.component.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;


/**
 * Created by Bruce on 2018/6/8.
 *
 * 將 矩形圖片 四個點 畫圓角（倒圓角）
 */
public class RoundRectangleImage extends AppCompatImageView {

    public static final int NO_ANGLE = 0;
    public static final int MACHINE_RECIPE_ROUND_ANGLE = 40;
    public static final int DEFAULT_ROUND_ANGLE = 100;

    private Paint m_paint;
    private int m_roundPx = DEFAULT_ROUND_ANGLE; // default

    //----------------------------------------------------------
    // Construct
    //----------------------------------------------------------
    public RoundRectangleImage(Context context) {
        this(context, null);
    }

    public RoundRectangleImage(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundRectangleImage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        m_paint = new Paint();
    }

    //----------------------------------------------------------
    // Super Method
    //----------------------------------------------------------
    @Override
    protected void onDraw(Canvas canvas) { // 畫圓角圖

        Drawable drawable = getDrawable();
        if (null == drawable) {
            super.onDraw(canvas);
            return;
        }

        //------------------------------------
        // start to draw
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        Bitmap roundBitmap = getRoundBitmap(bitmap, m_roundPx);

        final Rect rectSrc = new Rect(0, 0, roundBitmap.getWidth(), roundBitmap.getHeight());
        final Rect rectDest = new Rect(0, 0, getWidth(), getHeight());

        m_paint.reset();
        canvas.drawBitmap(roundBitmap, rectSrc, rectDest, m_paint);
    }

    //----------------------------------------------------------
    // Public Method
    //----------------------------------------------------------
    public void setRoundPx(int roundPx) {
        this.m_roundPx = roundPx;
    }

    //----------------------------------------------------------
    // Private Preference
    //----------------------------------------------------------
    private Bitmap getRoundBitmap(Bitmap bitmap, int roundPx) { //取得圓角矩形圖的方法

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        m_paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, roundPx, roundPx, m_paint);

        m_paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, m_paint);
        return output;
    }
}
