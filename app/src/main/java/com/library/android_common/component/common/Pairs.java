package com.library.android_common.component.common;

import com.library.android_common.constant.CsLog;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Ray on 11/06/2018.
 * 複數 Pair
 */

public class Pairs<K, V> {

    private ArrayList<Pair<K, V>> m_pairs;
    private Pairs<K, V> m_self;

    private IPredicate<K, V> m_predicate;
    private IConsumer<K, V> m_consumer;
    private IMap<K, V> m_iMap;

    //-----------------------------------------------------------
    // Construct
    //-----------------------------------------------------------
    public Pairs() {
        m_self = this;
        m_pairs = new ArrayList<>();
    }

    public Pairs(K k, V v) {
        this(Pair.of(k, v));
    }

    public Pairs(Pair<K, V>... ps) {
        this(Lst.of(ps).toList());
    }

    public Pairs(ArrayList<Pair<K, V>> ps) {
        m_self = this;
        m_pairs = ps;
    }

    //--------------------------------------------
    // static method
    public static <K, V> Pairs<K, V> of(K k, V v) {
        return of(Pair.of(k, v));
    }

    public static <K, V> Pairs<K, V> of(Pairs<K, V> ps) {
        return ps;
    }

    public static <K, V> Pairs<K, V> of(Pair<K, V>... ps) {
        return new Pairs(Lst.of(ps).toList());
    }

    public static <K, V> Pairs<K, V> of(ArrayList<Pair<K, V>> ps) {
        return new Pairs(ps);
    }

    //----------------------------------------------------------------------
    // test
    //----------------------------------------------------------------------
    public static void main(String[] args) {

    }

    private Pairs<K, V> self() {
        return Pairs.of(m_pairs);
    }

    //-----------------------------------------------------------
    // Public Method
    //-----------------------------------------------------------
    // Get
    public Opt<Pair<K, V>> optGet(int pos) {
        return Opt.of(get(pos));
    }

    public Pair<K, V> get(int pos) {
        return m_pairs.get(pos);
    }

    public Pair<K, V> getByKey(K k) {
        for (Pair<K, V> p : m_pairs)
            if (p.k().equals(k))
                return p;
        return null;
    }

    public ArrayList<V> getVsByKs(final K... ks) {
        return Pairs.of(m_pairs).removeIf(new IPredicate<K, V>() {
            @Override
            public boolean remove(K k, V v) {
                return !Lst.of(ks).contains(k);
            }
        }).toVList();
    }

    public Pair<K, V> getByVal(V v) {
        for (Pair<K, V> p : m_pairs)
            if (p.v().equals(v) || p.v() == v)
                return p;
        return null;
    }

    public Pair<K, V> first() {
        return m_pairs.get(0);
    }

    public Pair<K, V> last() {
        return m_pairs.get(m_pairs.size() - 1);
    }

    public K getKeyByVal(V v) {
        return getByVal(v).k();
    }

    public Opt<K> optGetKeyByVal(V v) {
        return Opt.of(getKeyByVal(v));
    }

    public V getValbyKey(K k) {
        return getByKey(k).v();
    }

    public Opt<V> optGetValByKey(K k) {
        return Opt.of(getValbyKey(k));
    }

    //-----------------------------------------
    // add
    public Pairs<K, V> add(K k, V v) {
        add(Pair.of(k, v));
        return m_self;
    }

    public Pairs<K, V> add(Pair<K, V> p) {
        m_pairs.add(p);
        return m_self;
    }

    public Pairs<K, V> addAll(Pairs<K, V> ps) {
        m_pairs.addAll(ps.toList());
        return m_self;
    }

    public Pairs<K, V> addAll(ArrayList<Pair<K, V>> ps) {
        m_pairs.addAll(ps);
        return m_self;
    }

    public Pairs<K, V> setKAll(K... ks) {
        return setKAll(Lst.of(ks).toList());
    }

    public Pairs<K, V> setKAll(final Collection<K> ks) {
        return self().forEach(new IConsumer<K, V>() {
            @Override
            public void runEach(int i, Pair<K, V> p) {
                p.setK(Lst.of(ks).get(i));
            }
        });
    }

    public Pairs<K, V> setVAll(V... vs) {
        return setVAll(Lst.of(vs).toList());
    }

    public Pairs<K, V> setVAll(final Collection<V> vs) {
        return self().forEach(new IConsumer<K, V>() {
            @Override
            public void runEach(int i, Pair<K, V> p) {
                p.setV(Lst.of(vs).get(i));
            }
        });
    }

    //-----------------------------------------
    // remove
    public Pairs<K, V> remove(int position) {
        m_pairs.remove(position);
        return m_self;
    }

    public Pairs<K, V> removeByKey(K k) {
        for (int i = m_pairs.size() - 1; i >= 0; i--) {
            if (m_pairs.get(i).k().equals(k))
                m_pairs.remove(i);
        }
        return m_self;
    }

    public Pairs<K, V> removeByVal(V v) {
        for (int i = m_pairs.size() - 1; i >= 0; i--) {
            if (m_pairs.get(i).v().equals(v))
                m_pairs.remove(i);
        }
        return m_self;
    }

    public Pairs<K, V> removeIf(IPredicate<K, V> predicate) {
        m_predicate = predicate;
        for (int i = m_pairs.size() - 1; i >= 0; i--) {
            Pair<K, V> p = m_pairs.get(i);
            if (m_predicate.remove(p.k(), p.v()))
                m_pairs.remove(i);
        }
        return m_self;
    }

    //-----------------------------------------
    // other
    public Pairs<K, V> forEach(IConsumer<K, V> consumer) {
        m_consumer = consumer;
        for (int i = 0; i < m_pairs.size(); i++) {
            m_consumer.runEach(i, m_pairs.get(i));
        }
        return m_self;
    }

    public Pairs<K, V> backForEach(IConsumer<K, V> consumer) {
        m_consumer = consumer;
        for (int i = m_pairs.size() - 1; i >= 0; i--) {
            m_consumer.runEach(i, m_pairs.get(i));
        }
        return m_self;
    }

    public Pairs<K, V> map(IMap<K, V> iMap) {
        m_iMap = iMap;
        for (int i = 0; i < m_pairs.size(); i++) {
            m_pairs.set(i, m_iMap.mapEach(m_pairs.get(i).k(), m_pairs.get(i).v()));
        }
        return m_self;
    }

    public Lst<K> toKLst() {
        return Lst.of(toKList());
    }

    public Lst<V> toVLst() {
        return Lst.of(toVList());
    }

    public ArrayList<K> toKList() {
        final ArrayList<K> kLst = new ArrayList<>();
        Lst.of(m_pairs).forEach(new Lst.IConsumer<Pair<K, V>>() {
            @Override
            public void runEach(int i, Pair<K, V> p) {
                kLst.add(p.k());
            }
        });
        return kLst;
    }

    public ArrayList<V> toVList() {
        final ArrayList<V> vLst = new ArrayList<>();
        Lst.of(m_pairs).forEach(new Lst.IConsumer<Pair<K, V>>() {
            @Override
            public void runEach(int i, Pair<K, V> p) {
                vLst.add(p.v());
            }
        });
        return vLst;
    }

    public Pairs<K, V> clear() {
        m_pairs.clear();
        return m_self;
    }

    public Pairs<K, V> reverse() {
        Collections.reverse(m_pairs);
        return m_self;
    }

    public Pairs<K, V> distinct() {
        return Pairs.of(self().toLst().distinct().toList());
    }

    public Pairs<K, V> swapAll() {
        ArrayList<Pair<K, V>> ps = new ArrayList<>();
        for (Pair<K, V> p : toList())
            ps.add(p.swap());
        m_pairs = ps;
        return m_self;
    }

    public int size() {
        return m_pairs.size();
    }

    public String toString() {
        ArrayList<String> ps = new ArrayList<>();
        for (Pair<K, V> p : toList())
            ps.add(p.toString());
        return StrUtil.builder(StrUtil.COMMA, ps);
    }

    public boolean isEmpty() {
        return m_pairs.size() == 0;
    }

    public boolean contains(Pair<K, V> p) {
        for (Pair<K, V> item : m_pairs)
            if (item.compareTo(p))
                return true;
        return false;
    }

    //--------------------------------------------------
    // convert to
    public ArrayList<Pair<K, V>> toList() {
        return m_pairs;
    }

    public Lst<Pair<K, V>> toLst() {
        return Lst.of(m_pairs);
    }

    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println_d(String filter) {
        int index = 0;
        for (Pair<K, V> p : toList()) {
            CsLog.d(Pairs.class, filter.concat(index + StrUtil.PERIOD + p.toString()));
            index += 1;
        }
    }

    public void println() { // for Java
        System.out.println(toString());
    }

    //----------------------------------------------------------------------
    // Interface
    //----------------------------------------------------------------------
    public interface IPredicate<K, V> {
        boolean remove(K k, V v);
    }

    public interface IConsumer<K, V> {
        void runEach(int i, Pair<K, V> p);
    }

    public interface IMap<K, V> {
        Pair<K, V> mapEach(K k, V v);
    }


}
