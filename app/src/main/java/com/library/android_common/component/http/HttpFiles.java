package com.library.android_common.component.http;

import android.os.AsyncTask;

import com.library.android_common.component.common.Pair;
import com.library.android_common.util.StrUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * Create By Ray on 2018/5/16
 * 這個是結合 AsyncTask 的版本
 * 參考網址 : http://www.codejava.net/java-se/networking/upload-files-by-sending-multipart-request-programmatically
 */
public class HttpFiles extends AsyncTask<Pair<String, Pair<List<Pair<String, String>>, Pair<String, File>>>, Void, Pair<String, JSONObject>> {

    @Override
    protected Pair<String, JSONObject> doInBackground(Pair<String, Pair<List<Pair<String, String>>, Pair<String, File>>>... pairs) { // Pair <URL, Pair< Pair1_s<param key, param value>, Pair2<file_param_key, file>       >

        //----------------------------------------------------------------
        // declare
        Pair<String, Pair<List<Pair<String, String>>, Pair<String, File>>> p = pairs[0];
        String api_url = p.k(); // 第一個 key 為 api_url
        List<Pair<String, String>> params = p.v().k(); // 各個 params k & v
        Pair<String, File> fileParam = p.v().v(); // param & file

        JSONObject jsObj = null;
        //----------------------------------------------------------------
        // http request
        try {
            //-------------------------------------
            HttpFileUploader httpLoader = new HttpFileUploader(api_url, StrUtil.UTF_8);
            httpLoader.addFile(fileParam.k(), fileParam.v());
            for (Pair<String, String> param : params)
                httpLoader.addParam(param.k(), param.v());

            jsObj = httpLoader.getJSONObject();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Pair.of(jsObj.toString(), jsObj);
    }

}
