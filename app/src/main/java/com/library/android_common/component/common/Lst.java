package com.library.android_common.component.common;

import com.library.android_common.constant.CsLog;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.StrUtil;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Ray on 13/06/2018.
 */

public class Lst<E> {

    private ArrayList<E> m_lst;
    private Lst m_self;

    private IPredicate<E> m_ipredicate;
    private IConsumer<E> m_iconsumer;
    private IMap<E> m_imap;
    private IConvert m_iconvert;

    //---------------------------------------------
    // Construct
    //---------------------------------------------
    public Lst() {
        init();
    }

    public Lst(Iterable<E> lst) {
        init();
        for (E e : lst)
            m_lst.add(e);
    }

    public Lst(Collection<E> lst) {
        init();
        for (E e : lst)
            m_lst.add(e);
    }

    public Lst(Enumeration<E> lst) {
        init();
        while (lst.hasMoreElements())
            m_lst.add(lst.nextElement());
    }

    public Lst(Iterator<E> iterator) {
        init();
        while (iterator.hasNext())
            m_lst.add(iterator.next());
    }

    public Lst(E... es) {
        init();
        for (int i = 0; i < es.length; i++)
            m_lst.add(es[i]);
    }

    //---------------------------------------------
    // Static Method
    static public <E> Lst<E> of(Iterable<E> iterable) {
        return new Lst<>(iterable);
    }

    static public <E> Lst<E> of(Collection<E> collection) {
        return new Lst<>(collection);
    }

    static public <E> Lst<E> of(Enumeration<E> e) {
        return new Lst<>(e);
    }

    static public <E> Lst<E> of(Iterator<E> iterator) {
        return new Lst<>(iterator);
    }

    static public <E> Lst<E> of(E... es) {
        return new Lst<>(es);
    }

    static public Lst<String> splitOf(String content, String regex) {
        return Lst.of(content.split(regex));
    }

    static public Lst<String> splitOfComma(String content) {
        return splitOf(content, StrUtil.COMMA);
    }

    //-----------------------------------------------
    // Test
    //-----------------------------------------------
    public static void main(String[] args) {

//        String s = Lst.of(11, 22, 33, 44).getOr(2, 55).toString();
//        JavaTools.println(s);

        Lst.of(Lst.of("1", "2", "3").optFind("4").notPosEmpty().getOr(1)).println();


    }

    private Lst<E> self() {
        return Lst.of(m_lst);
    }

    private void init() {
        m_self = this;
        m_lst = new ArrayList<>();
    }

    //----------------------------------------------------------------
    // Public Method
    //----------------------------------------------------------------
    // Get
    public Opt<E> optGet(int pos) {
        E e = null;
        try {
            e = get(pos);
        } catch (IndexOutOfBoundsException ex) {
            ex.printStackTrace();
        }
        return Opt.of(e);
    }

    public E get(int pos) {
        return m_lst.get(pos);
    }

    public E getOr(int pos, E e) {
        return optGet(pos).getOr(e);
    }

    public E first() {
        return m_lst.get(0);
    }

    public E last() {
        return m_lst.get(m_lst.size() - 1);
    }

    public int find(E e) {
        int notFound = -1;
        for (int i = 0; i < m_lst.size(); i++) {
            if (e.equals(m_lst.get(i)) || e == m_lst.get(i))
                return i;
        }
        return notFound;
    }

    public Opt<Integer> optFind(E e) {
        return Opt.of(find(e));
    }

    public ArrayList<Integer> finds(E... es) {
        return finds(Lst.of(es).toList());
    }

    public ArrayList<Integer> finds(ArrayList<E> es) {
        ArrayList<Integer> items = new ArrayList<>();
        for (int i = 0; i < es.size(); i++) {
            for (int j = 0; j < m_lst.size(); j++) {
                if (es.get(i).equals(m_lst.get(i)) || es.get(i) == m_lst.get(i))
                    items.add(i);
            }
        }
        return items;
    }

    //----------------------------------------------------------------
    // add
    public Lst<E> add(E e) {
        m_lst.add(e);
        return m_self;
    }

    public Lst<E> add(int pos, E e) {
        m_lst.add(pos, e);
        return m_self;
    }

    public Lst<E> addFirst(E e) {
        m_lst.add(0, e);
        return m_self;
    }

    public Lst<E> addAll(E... es) {
        addAll(Lst.of(es).toList());
        return m_self;
    }

    public Lst<E> addAll(Collection<E> coll) {
        m_lst.addAll(coll);
        return m_self;
    }

    //----------------------------------------------------------------
    // remove
    public Lst<E> remove(int pos) {
        m_lst.remove(pos);
        return m_self;
    }

    public Lst<E> removeBy(E... es) {
        removeBy(Lst.of(es).toList());
        return m_self;
    }

    public Lst<E> removeBy(ArrayList<E> items) {
        for (int i = 0; i < items.size(); i++)
            for (int j = m_lst.size() - 1; j >= 0; j--) {
                if (items.get(i).equals(m_lst.get(j)))
                    m_lst.remove(j);
            }
        return m_self;
    }

    public Lst<E> removeIf(IPredicate<E> iPredicate) {
        m_ipredicate = iPredicate;
        for (int i = m_lst.size() - 1; i >= 0; i--) {
            if (m_ipredicate.remove(m_lst.get(i)))
                m_lst.remove(i);
        }
        return m_self;
    }

    public Lst<E> removeTo(E e) { // ㄧ直 remove 直到指定 E 物件
        return removeTo(self().find(e));
    }

    public Lst<E> removeTo(final int pos) { // ㄧ直 remove 直到指定 position
        self().reverse().backForEach(new IConsumer<E>() {
            @Override
            public void runEach(int i, E e) {
                if (pos >= i + 1)
                    remove(i);
            }
        }).reverse();
        return m_self;
    }

    public Lst<E> backRemoveTo(E e) { // 從 back ㄧ直 remove 直到指定 E 物件
        return backRemoveTo(self().find(e));
    }

    public Lst<E> backRemoveTo(int pos) {  // 從 back 一直 remove 直到指定 position
        return self()
                .reverse()
                .removeTo(pos)
                .reverse();
    }

    // 輸入數個positions然後將輸入的positions去remove掉
    public Lst<E> removes(int... na) {

        return m_self;
    }

    //----------------------------------------------------------------
    // loop
    public Lst<E> forEach(IConsumer<E> iConsumer) {
        m_iconsumer = iConsumer;
        for (int i = 0; i < m_lst.size(); i++) {
            m_iconsumer.runEach(i, m_lst.get(i));
        }
        return m_self;
    }

    public Lst<E> backForEach(IConsumer<E> iConsumer) {
        m_iconsumer = iConsumer;
        for (int i = m_lst.size() - 1; i >= 0; i--) {
            m_iconsumer.runEach(i, m_lst.get(i));
        }
        return m_self;
    }

    public Lst<E> map(IMap<E> iMap) {
        m_imap = iMap;
        for (int i = 0; i < m_lst.size(); i++) {
            m_lst.set(i, m_imap.mapEach(i, m_lst.get(i)));
        }
        return m_self;
    }

    public <Out> Lst<Out> convert(IConvert<E, Out> iConvert) { // TODO finish
        m_iconvert = iConvert;
        ArrayList<Out> outs = new ArrayList<>();
        for (int i = 0; i < m_lst.size(); i++)
            outs.add((Out) m_iconvert.convert(m_lst.get(i)));
        return Lst.of(outs);
    }

    public ArrayList<String[]> splitAllOf(final String symbol) {
        final ArrayList<String[]> items = new ArrayList<>();
        Lst.of(m_lst).forEach(new IConsumer<E>() {
            @Override
            public void runEach(int i, E e) {
                items.add(e.toString().split(symbol));
            }
        });
        return items;
    }

    public Lst<E> sub(int fromIndex) {
        return sub(fromIndex, m_lst.size());
    }

    public Lst<E> sub(int fromIndex, int toIndex) {
        m_lst = Lst.of(m_lst.subList(fromIndex, toIndex)).toList();
        return m_self;
    }

    public Lst<E> pick(int... items) {
        ArrayList<E> es = new ArrayList<>();
        for (int pos : items)
            es.add(m_lst.get(pos));
        m_lst = es;
        return m_self;
    }

    public Lst<E> reverse() {
        Collections.reverse(m_lst);
        return m_self;
    }

    //----------------------------------------------------------------
    // basic
    public int size() {
        return m_lst.size();
    }

    public Lst<E> clear() {
        m_lst.clear();
        return m_self;
    }

    public Lst<E> distinct() {
        HashSet<E> set = new HashSet<>();
        for (E e : m_lst)
            set.add(e);
        m_lst = Lst.of(set).toList();
        return m_self;
    }

    public boolean contains(E e) {
        return m_lst.contains(e);
    }

    public boolean arrContains(E[] es) {
        for (E e : es)
            if (m_lst.contains(e))
                return true;
        return false;
    }

    public int indexOf(E e) {
        return m_lst.indexOf(e);
    }

    public int lastIndexof(E e) {
        return m_lst.lastIndexOf(e);
    }

    public Lst<String> trims() {
        final ArrayList<String> as = new ArrayList<>();
        self().forEach(new IConsumer<E>() {
            @Override
            public void runEach(int i, E e) {
                as.add(e.toString().trim());
            }
        });
        return Lst.of(as);
    }

    public String toString() {
        return toString(StrUtil.EMPTY);
    }

    public String toString(String middle) {
        return StrUtil.builder(middle, m_lst);
    }

    public ArrayList<E> toList() {
        return m_lst;
    }

    public E[] toArray() {
        return m_lst.size() == 0 ? (E[]) m_lst.toArray() : (E[]) Array.newInstance(m_lst.get(0).getClass(), m_lst.size());
    }

    public Collection<E> toColls() {
        Collection<E> coll = new ArrayList<>(m_lst);
        return coll;
    }

    public Lst<Opt<E>> toOpts() {
        ArrayList<Opt<E>> optItems = new ArrayList<>();
        for (E e : m_lst)
            optItems.add(Opt.of(e));
        return Lst.of(optItems);
    }

    //----------------------------------------------------------------
    // println
    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println_d(String filter) {
        for (int i = 0; i < m_lst.size(); i++) {
            CsLog.d(Lst.class, filter.concat(i + StrUtil.PERIOD + StrUtil.bracketsOf(m_lst.get(i).toString())));
        }
    }

    public Lst<E> println() { // for java
        int num = 0;
        for (E e : m_lst) {
            JavaTools.println(num + StrUtil.PERIOD + StrUtil.SPACE + e.toString());
            num++;
        }
        return m_self;
    }

    //----------------------------------------------------------------
    // Interface
    //----------------------------------------------------------------
    public interface IPredicate<E> {
        boolean remove(E e);
    }

    public interface IConsumer<E> {
        void runEach(int i, E e);
    }

    public interface IMap<E> {
        E mapEach(int i, E e);
    }

    public interface IConvert<E, Out> {
        Out convert(E e);
    }
}















