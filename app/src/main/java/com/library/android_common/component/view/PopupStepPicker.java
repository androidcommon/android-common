package com.library.android_common.component.view;

/**
 * Created by Raymond Zorro on 04/05/2018.
 * PopUp Step Picker
 */
// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class PopupStepPicker implements View.OnClickListener {
//
//
//    private Activity m_activity;
//    private PopupWindow m_popupView;
//    private View m_parentView;
//    private ViewHolder m_viewholder;
//    private AddStepsListener m_listener;
//
//    public static int m_nTotalVolume = 110; // 總水量不可以超過 1100 毫升，這裏使用 110 是因為原作者的邏輯運算少一個 0
//
//    //------------------------------------------------------------
//    // construct
//    //------------------------------------------------------------
//    public PopupStepPicker(Activity activity, View vParent) {
//        this.m_activity = activity;
//        this.m_parentView = vParent;
//
//        //-----------------------------------------------------------
//        // declare
//        View view = LayoutInflater.from(activity).inflate(R.layout.dialog_my_formula_add_formula_step, null);
//        m_popupView = new PopupWindow(view, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        m_popupView.setAnimationStyle(R.style.BottomFade);
//
//        m_viewholder = new ViewHolder(
//                (Button) view.findViewById(R.id.dialog_my_formula_add_formula_step_add_button),
//                (Button) view.findViewById(R.id.dialog_my_formula_add_formula_step_close_button),
//                (NumberPicker) view.findViewById(R.id.dialog_my_formula_add_formula_step_water_used_numberPicker),
//                (NumberPicker) view.findViewById(R.id.dialog_my_formula_add_formula_step_speed_numberPicker),
//                (NumberPicker) view.findViewById(R.id.dialog_my_formula_add_formula_step_interval_numberPicker)
//        );
//
//        ((TextView) view.findViewById(R.id.comp_dialog_water_title)).setText(MutiMsg.CLOUD_SEARCH_FORMULA_TEXT_WATER.msg());
//        ((TextView) view.findViewById(R.id.comp_dialog_speed_title)).setText(MutiMsg.SPEED_LV.msg());
//        ((TextView) view.findViewById(R.id.comp_dialog_speed_interval)).setText(MutiMsg.INTERVAL.msg());
//
//        //-----------------------------------------------------------
//        // set data
//        m_viewholder.btnConfirm.setText(MutiMsg.OK.msg());
//        m_viewholder.btnCancel.setText(MutiMsg.CANCEL.msg());
//        m_viewholder.btnCancel.setOnClickListener(this);
//        m_viewholder.btnConfirm.setOnClickListener(this);
//
//        set();
//    }
//
//    //------------------------------------------------------------
//    // public method
//    //------------------------------------------------------------
//    public void show(AddStepsListener listener) {
//        set();
//        m_listener = listener;
//        m_popupView.showAtLocation(m_parentView, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
//    }
//
//    public void dismiss() {
//        m_popupView.dismiss();
//    }
//
//    public PopupWindow getInstance() {
//        return m_popupView;
//    }
//
//    //------------------------------------------------------------
//    // implement method
//    //------------------------------------------------------------
//    @Override
//    public void onClick(View view) {
//
//        switch (view.getId()) {
//
//            case R.id.dialog_my_formula_add_formula_step_add_button: // confirm
//
//                m_nTotalVolume = m_nTotalVolume - m_viewholder.waterPicker.getValue();
//                int waterUsed = m_viewholder.waterPicker.getValue() * 10;
//                int speed = m_viewholder.speedPicker.getValue();
//                int interval = m_viewholder.intervalPicker.getValue();
//
//                FormulaStepModel step = new FormulaStepModel("Popup Picker", waterUsed, speed, interval);
//                FormulaEditorActivity.getInstance().sendMessage(CommandConstant.CMD_REFRESH_STEP_LIST);
//                m_listener.addSteps(step, m_nTotalVolume <= 0);
//
//                m_popupView.setOutsideTouchable(true);
//                AlphaBackground.normal(m_activity);
//                dismiss();
//
//                break;
//
//            case R.id.dialog_my_formula_add_formula_step_close_button: // cancel
//
//                m_popupView.setOutsideTouchable(true);
//                AlphaBackground.normal(m_activity);
//                dismiss();
//
//                break;
//
//
//        }
//
//        String s = "";
//        ArrayList sa = Lst.of(s).toList();
//        sa.get(0);
//        sa.toString();
//
//    }
//
//    private void set() {
//
//        String waterUsedValues[] = new String[(m_nTotalVolume > 70) ? 70 : m_nTotalVolume + 1]; // 原作者的code
//        for (int i = 0; i < waterUsedValues.length; i++) {
//            String number = Integer.toString(i * 10);
//            waterUsedValues[i] = number.length() < 2 ? "0" + number : number;
//        }
//
//        m_viewholder.waterPicker.setMinValue(0);
//        m_viewholder.waterPicker.setMaxValue((m_nTotalVolume > 69) ? 69 : m_nTotalVolume);
//        m_viewholder.waterPicker.setDisplayedValues(waterUsedValues);
//
//        m_viewholder.speedPicker.setMinValue(1);
//        m_viewholder.speedPicker.setValue(1);
//        m_viewholder.speedPicker.setMaxValue(15);
//
//        m_viewholder.intervalPicker.setMinValue(0);
//        m_viewholder.intervalPicker.setValue(1);
//        m_viewholder.intervalPicker.setMaxValue(60);
//    }
//
//    //------------------------------------------------------------
//    // Listener
//    //------------------------------------------------------------
//    public interface AddStepsListener {
//        void addSteps(FormulaStepModel fsm, boolean isFull);
//    }
//
//    //------------------------------------------------------------
//    // ViewHolder
//    //------------------------------------------------------------
//    class ViewHolder {
//
//        private Button btnConfirm, btnCancel;
//        private NumberPicker waterPicker, speedPicker, intervalPicker;
//
//        ViewHolder(Button btnConfirm, Button btnCancel, NumberPicker wPicker, NumberPicker sPicker, NumberPicker idPicker) {
//            this.waterPicker = wPicker;
//            this.speedPicker = sPicker;
//            this.intervalPicker = idPicker;
//            this.btnCancel = btnCancel;
//            this.btnConfirm = btnConfirm;
//        }
//    }
//
//}
