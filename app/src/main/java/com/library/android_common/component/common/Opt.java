package com.library.android_common.component.common;


import com.library.android_common.constant.CsLog;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;
import java.util.Collection;


/**
 * Create By Ray on 2018/5/14
 * To set、get default value in diff type, Where data is from api.
 * Regular Expression
 */
public class Opt<T> {

    private static String[] opts = {StrUtil.STR_NULL, "-1"};
    private T m_t;
    private boolean m_boolean;
    private Opt<T> m_self;
    private ArrayList<T> m_ts;

    //-------------------------------------------------------
    // Construct
    //-------------------------------------------------------
    public Opt(T t) {
        this.m_t = t;
        this.m_ts = new ArrayList<>();
        m_self = this;
    }

    static public <T> Opt<T> of(T t) {
        return new Opt<>(t);
    }

    //-------------------------------------------------------
    // Int
    //-------------------------------------------------------
    static public int getInt(int value) {
        return getInt(String.valueOf(value));
    }

    static public int getInt(String val) {
        return Opt.of(val)
                .notStrEmpty()
                .not(opts)
                .optGetOr(StrUtil.ZERO)
                .parseToInt()
                .get();
    }

    //-------------------------------------------------------
    // Long
    //-------------------------------------------------------
    static public long getLong(long value) {
        return getInt((int) value);
    }

    static public long getLong(String value) {
        return getInt(value);
    }

    //-------------------------------------------------------
    // Double
    //-------------------------------------------------------
    static public double getDouble(double value) {
        return getDouble(String.valueOf(value));
    }

    static public double getDouble(String val) {
        return Opt.of(val)
                .notStrEmpty()
                .not(opts)
                .optGetOr(StrUtil.EMPTY)
                .parseToDouble()
                .get();
    }

    //-------------------------------------------------------
    // String
    //-------------------------------------------------------
    static public String getString(String val) {
        return Opt.of(val)
                .optGetOr(StrUtil.EMPTY)
                .not(opts)
                .get();
    }

    public static void main(String[] args) {


        String s = "1d,23as,d4s,a52f,2341,das2321da,fa23,sf231,as5,--123123,--1//d";
        Opt.of(s).splitToLst("-")
                .optGet(Lst.splitOfComma(s)
                        .optFind("3")
                        .notPosEmpty()
                        .get());


        s = s.replaceAll("[^0-9]", StrUtil.EMPTY);

        if (s.equals("3"))
            s = "";
        else
            s = "2";

        int num = Integer.parseInt(s);

        if (num == 3)
            num = 5;

        double dnum = Double.parseDouble(num + "");

        Opt.of(s).numOnly()
                .not("3")
                .optGetOr("2")
                .parseToInt()
                .not(3)
                .optGetOr(5)
                .parseToDouble()
                .get();

        //---------------------------------------------------------

    }

    //-------------------------------------------------------
    // Generic
    //-------------------------------------------------------
    public T get() {
        return m_t;
    }

    public T getOr(T t) {
        if (null == m_t)
            return t;

        for (T item : m_ts)
            if (item.equals(m_t) || item == m_t)
                return t;

        return m_t;
    }

    public Opt<T> optGet() {
        return Opt.of(get());
    }

    public Opt<T> optGetOr(T t) {
        return Opt.of(getOr(t));
    }

    public Opt<T> not(Collection<T> ts) {
        m_ts.addAll(ts);
        return m_self;
    }

    public Opt<T> not(T... ts) {
        m_ts = Lst.of(ts).toList();
        return m_self;
    }

    public Opt<String> notStrEmpty() {
        return Opt.of(m_t.toString()).not(StrUtil.EMPTY);
    }

    public Opt<Integer> notPosEmpty() {
        return Opt.of(toString())
                .parseToInt()
                .not(-1);
    }

    //----------------------------------------------------
    // type
    public String toString() {
        return m_ts.toString();
    }

    public Opt<String> toOptString() {
        return Opt.of(toString());
    }

    public Opt<String> trim() {
        return Opt.of(toString().trim());
    }

    public Opt<String> concat(String content) {
        return Opt.of(toString().concat(content));
    }

    public Opt<String> replace(String sOld, String sNew) {
        return Opt.of(toString().replace(sOld, sNew));
    }

    public Opt<String> replaceAll(String regex, String replacement) {
        return Opt.of(toString().replaceAll(regex, replacement));
    }

    public Opt<String> subStr(int begin) {
        return Opt.of(toString().substring(begin));
    }

    public Opt<String> subStr(int begin, int end) {
        return Opt.of(toString().substring(begin, end));
    }

    public Opt<String> subIndexOf(String regex) {
        return Opt.of(toString()).subStr(toString().indexOf(regex));
    }

    public Opt<String> subIndexOf(String from, String end) {
        return Opt.of(toString().substring(toString().indexOf(from), toString().indexOf(end)));
    }

    public Opt<String[]> split(String regex) {
        return Opt.of(toString().split(regex));
    }

    public Lst<Opt<String>> splitToLst(String regex) {
        return Lst.of(toString().split(regex)).toOpts();
    }

    public Lst<Opt<String>> splitOfCommaToLst() {
        return Lst.of(toString().split(StrUtil.COMMA)).toOpts();
    }

    public Opt<String[]> splitOfComma() {
        return split(StrUtil.COMMA);
    }

    public Opt<Integer> strIndexOf(String regex) {
        return Opt.of(toString().indexOf(regex));
    }

    public Opt<Integer> strLastIndexOf(String regex) {
        return Opt.of(toString().lastIndexOf(regex));
    }

    public Opt<String> contains(String content, String whenTrue, String whenFalse) {
        return Opt.of(toString().contains(content) ? whenTrue : whenFalse);
    }

    public Opt<String> hasStr(String content, String sTrue) {
        return contains(content, sTrue, toString());
    }

    //-------------------------------------------------------
    // Regular Expression

    public Opt<String> hasNotStr(String content, String sFalse) {
        return contains(content, toString(), sFalse);
    }

    public Opt<Double> parseToDouble() {
        double val = 0.0;
        try {
            val = Double.parseDouble(m_t.toString());
        } catch (NumberFormatException e) {
            CsLog.e(Opt.class, "NumberFormatException parseDouble : " + e.getMessage());
            e.printStackTrace();
        }
        return Opt.of(val);
    }

    public Opt<Integer> parseToInt() {
        int val = 0;
        try {
            val = Integer.parseInt(m_t.toString());
        } catch (NumberFormatException e) {
            CsLog.e(Opt.class, "NumberFormatException parseInt : " + e.getMessage());
            e.printStackTrace();
        }
        return Opt.of(val);
    }

    public Opt<Long> parseToLong() {
        long val = 0;
        try {
            val = Long.parseLong(m_t.toString());
        } catch (NumberFormatException e) {
            CsLog.e(Opt.class, "NumberFormatException parseLong : " + e.getMessage());
            e.printStackTrace();
        }
        return Opt.of(val);
    }

    public Opt<Float> parseToFloat() {
        float val = 0;
        try {
            val = Float.parseFloat(m_t.toString());
        } catch (NumberFormatException e) {
            CsLog.e(Opt.class, "NumberFormatException parseFloat : " + e.getMessage());
            e.printStackTrace();
        }
        return Opt.of(val);
    }

    public void println() {
        JavaTools.println(toString());
    }

    public void println_d(String filter) {
        CsLog.d(Opt.class, filter.concat(toString()));
    }

    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public Opt<String> numOnly() {
        return Opt.of(m_t.toString().replaceAll("[^0-9]", StrUtil.EMPTY));
    }

    public Opt<String> azOnly() {
        return Opt.of(m_t.toString().replaceAll("[^a-zA-Z]", StrUtil.EMPTY));
    }

    public String regularEx(String... exs) { // TODO 待完成
        return m_t.toString()
                .replaceAll(StrUtil.bracketsOf(StrUtil.doubleQuoteOf(Lst.of(exs).toString())), StrUtil.EMPTY);
    }

}