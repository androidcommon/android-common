package com.library.android_common.component.view.ios_style;

/**
 * Create By Ray on 2018/6/14
 * 管理與 ios 相似的 component
 */
public class IOSTheme {

    //------------------------------------------------------------------------
    // Color
    //------------------------------------------------------------------------
    public enum Color{
        TEXT_BLUE("#037BFF"),
        TEXT_BLACK("#000000");

        //---------------------------------------
        private String m_sColorCode;

        Color(String sColorCode){
            m_sColorCode = sColorCode;
        }
        //---------------------------------------
        public String code(){
            return m_sColorCode;
        }
    }


}
