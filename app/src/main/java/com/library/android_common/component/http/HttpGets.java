package com.library.android_common.component.http;

import android.os.AsyncTask;
import android.util.Log;

import com.library.android_common.component.common.Pair;
import com.library.android_common.enums.HttpDef;
import com.library.android_common.util.JSONUtil;
import com.library.android_common.util.StrUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Raymond Zorro on 08/05/2018.
 * To use AsyncTask redirect call api thread to UI main thread
 */

public class HttpGets extends AsyncTask<Pair<String, String>, Void, Pair<String, JSONObject>> {

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Pair<String, JSONObject> doInBackground(Pair<String, String>... ps) { // 這裡只需要透過 key 去拿 url, 但傳入物件使用 pair 是因為多一點彈性

        String sUrl = ps[0].v().trim();
        String sRlt = StrUtil.EMPTY;

        try {
            HttpResponse response = new DefaultHttpClient()
                    .execute(new org.apache.http.client.methods.HttpGet(sUrl));
            HttpEntity resEntity = response.getEntity();
            sRlt = EntityUtils.toString(resEntity);

        } catch (IOException e) {
            Log.e(HttpDef.class.getName(), "Error" + " error msg : " + e.getMessage());
            e.printStackTrace();
        }

        return Pair.of(sRlt, JSONUtil.toJSObj(sRlt));
    }
}
