package com.library.android_common.component;

import android.os.CountDownTimer;

/**
 * Created by Raymond Zorro on 03/05/2018.
 * 計時器
 */

public class TimeCounter {

    static private onTimeUpListener m_listener;

    //---------------------------------------
    // public method
    //---------------------------------------
    static public void countDown(int second, onTimeUpListener listener) {
        m_listener = listener;
        new CountDownTimer(second * 1000, 1000) {

            @Override
            public void onFinish() {
                m_listener.onFinish();
            }

            @Override
            public void onTick(long m) {

            }
        }.start();
    }

    static public void countDown(double second, onTimeUpListener listener) {
        m_listener = listener;
        new CountDownTimer((int) (second * 1000), 1000) {

            @Override
            public void onFinish() {
                m_listener.onFinish();
            }

            @Override
            public void onTick(long m) {

            }
        }.start();
    }

    static public void delay(int second) {
        countDown(second, new onTimeUpListener() {
            @Override
            public void onFinish() {
                // do nothing
            }
        });
    }

    static public void delay(long second) {
        delay(second);
    }

    //---------------------------------------
    // listener
    //---------------------------------------
    public interface onTimeUpListener {
        void onFinish();
    }

}
