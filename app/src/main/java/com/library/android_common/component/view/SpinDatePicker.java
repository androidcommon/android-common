package com.library.android_common.component.view;

import android.content.Context;

import com.library.android_common.util.StrUtil;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

/**
 * Created by Raymond Zorro on 24/04/2018.
 * Custom Picker
 */

public class SpinDatePicker implements DatePickerDialog.OnDateSetListener {

    private SpinnerDatePickerDialogBuilder dateBuilder;
    private YMDListener ymdListener;

    //------------------------------------
    public SpinDatePicker(Context context) {
        dateBuilder = new SpinnerDatePickerDialogBuilder();
        dateBuilder.context(context)
                .spinnerTheme(DatePickerDialog.THEME_HOLO_DARK)
                .callback(this);
    }

    public SpinDatePicker(Context context, YMDListener ymdListener) {
        this.ymdListener = ymdListener;
        dateBuilder = new SpinnerDatePickerDialogBuilder();
        dateBuilder.context(context)
                .spinnerTheme(DatePickerDialog.THEME_HOLO_DARK)
                .callback(this);
    }
    //------------------------------------

    private String slashYMD(int y, int m, int d) {
        return StrUtil.of(StrUtil.SLASH, y, m, d);
    }

    private String slashYMD(String y, String m, String d) {
        return StrUtil.of(StrUtil.SLASH, y, m, d);
    }

    private String dashYMD(int y, int m, int d) {
        return StrUtil.of(StrUtil.DASH, y, m, d);
    }

    private String dashYMD(String y, String m, String d) {
        return StrUtil.of(StrUtil.DASH, y, m, d);
    }

    //------------------------------------

    public void showPicker(YMDListener ymdListener) {
        this.ymdListener = ymdListener;
        dateBuilder.build()
                .show();
    }

    public void showPicker() {
        dateBuilder.build()
                .show();
    }

    public void dissmiss() {
//        dateBuilder.dissmiss();
    }

    //------------------------------------
    @Override
    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1; // 此處 + 1 是因為 lib 的 bug, month 從 0 開始到 11。
        //-----------------------------------------------------------------
        String m = (monthOfYear + "".length() == 1) ? ("0".concat(monthOfYear + "")) : (monthOfYear + "");
        String d = (dayOfMonth + "".length() == 1) ? ("0".concat(dayOfMonth + "")) : (dayOfMonth + "");

        ymdListener.ofYMD(slashYMD(year + "", m, d),
                dashYMD(year + "", m, d));
    }

    //-------------------------------------------
    // listener
    public interface YMDListener {
        void ofYMD(String slashYMD, String dashYMD);
    }
}

