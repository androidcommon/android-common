package com.library.android_common.component.view.viewpager;

/**
 * Create By Ray on 2018/6/19
 * 有 Indicator 的 ViewPager
 */

// TODO View 的 component 因 resource 問題先暫時 mark 待日後轉 Java code 版本 by Bruce
//public class IndicatorViewPager extends LinearLayout {
//
//    private IndicatorViewPager m_self;
//    private ViewPager m_viewPager;
//    private CircleIndicator m_circleIndicator;
//    private ArrayList<View> m_views;
//    private FrameLayout m_frameLayout;
//
//    //----------------------------------------------------------
//    // Construct
//    //----------------------------------------------------------
//    public IndicatorViewPager(Context context) {
//        super(context);
//        init();
//    }
//
//    public IndicatorViewPager(Context context, @Nullable AttributeSet attrs) {
//        super(context, attrs);
//        init();
//    }
//
//    public IndicatorViewPager(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//        init();
//    }
//
//    private void init() {
//        m_self = this;
//        this.inflate(getContext(), R.layout.comp_viewpager_indicator, this);
//        this.m_frameLayout = (FrameLayout) findViewById(R.id.parent_frame_layout);
//        this.m_viewPager = (ViewPager) findViewById(R.id.comp_vp);
//        this.m_circleIndicator = (CircleIndicator) findViewById(R.id.comp_indicator);
//        this.m_views = new ArrayList<>();
//        this.m_views.clear();
//    }
//
//    //----------------------------------------------------------
//    // Public Method
//    //----------------------------------------------------------
//    public IndicatorViewPager addImgViews(ArrayList<View> views) {
//        this.m_views.addAll(views);
//        return m_self;
//    }
//
//    public IndicatorViewPager addImgView(View v) {
//        m_views.add(v);
//        return m_self;
//    }
//
//    public IndicatorViewPager addImageUrls(ArrayList<String> urls) {
//        Lst.of(urls).forEach(new Lst.IConsumer<String>() {
//            @Override
//            public void runEach(int i, String url) {
//
//                ImageView img = new ImageView(getContext());
//                img.setScaleType(ImageView.ScaleType.FIT_XY);
//                Picasso.get().load(url).into(img);
//
//                m_views.add(img);
//            }
//        });
//        return m_self;
//    }
//
//    public IndicatorViewPager addImageUrls(String... urls) {
//        addImageUrls(Lst.of(urls).toList());
//        return m_self;
//    }
//
//    public IndicatorViewPager avoidConnLess(ArrayList<Integer> vs) { // 無網路狀態下使用 default pic
//        if (NetworkManager.isConnected)
//            return m_self;
//        //-----------------------------------
//        Lst.of(vs).forEach(new Lst.IConsumer<Integer>() {
//            @Override
//            public void runEach(int i, Integer r) {
//
//                ImageView img = new ImageView(getContext());
//                img.setScaleType(ImageView.ScaleType.FIT_XY);
//                img.setImageResource(r);
//
//                m_views.add(img);
//            }
//        });
//        return m_self;
//    }
//
//    public IndicatorViewPager defaultView() { // 順序要注意
//        //----------------------------------------------------------
//        // declare
//        View defaultView = LayoutInflater.from(getContext()).inflate(R.layout.comp_samantha_banner_default_view, null);
//        ((TextView) defaultView.findViewById(R.id.samantha_notification_textView_not_connect_banner)).setText(MutiMsg.SAMANTHA_PAGE_NOT_CONNECTED_TITLE.msg());
//
//        //----------------------------------------------------------
//        // show connect alert
//        if (!BLEManager.getInstance().isConnected())
//            m_views.add(0, defaultView);
//
//        return m_self;
//    }
//
//    public IndicatorViewPager clear() {
//        m_views.clear();
//        return m_self;
//    }
//
//    public void retrieve() {
//
//        //----------------------------------------------
//        // 判斷 Indicator 需不需要出現，小於 1 筆不出現
//        if (m_views.size() <= 1)
//            m_circleIndicator.setVisibility(INVISIBLE);
//        else
//            m_circleIndicator.setVisibility(VISIBLE);
//
//        this.m_viewPager.setAdapter(pagerAdapter);
//        this.m_circleIndicator.setViewPager(m_viewPager);
//    }
//
//
//    //----------------------------------------------------------
//    // Adapter
//    //----------------------------------------------------------
//    private PagerAdapter pagerAdapter = new PagerAdapter() {
//        @Override
//        public int getCount() {
//            return m_views.size();
//        }
//
//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            return view == object;
//        }
//
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object) {
//            container.removeView((View) object);
//        }
//
//        @Override
//        public Object instantiateItem(ViewGroup container, int position) {
//            View v = null;
//            v = m_views.get(position);
//            container.addView(v);
//            return v;
//        }
//    };
//
//}

































