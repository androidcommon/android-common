package com.library.android_common.component.view.radarview;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;

import com.library.android_common.component.common.Lst;
import com.library.android_common.component.common.Pair;
import com.library.android_common.constant.CsLog;
import com.library.android_common.enums.MutiMsg;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

import rorbin.q.radarview.RadarData;
import rorbin.q.radarview.RadarView;

/**
 * Create By Ray on 2018/6/7
 * formula radar chart view
 */
public class RadarViewChart extends RadarView {


    private RadarView m_radarView;
    private final String COLOR_LAYER_LINE = "#b4b4b5";

    //----------------------------------------------------
    // Construct View
    //----------------------------------------------------
    public RadarViewChart(Context context) {
        super(context);
        init();
    }

    public RadarViewChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadarViewChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        m_radarView = this;
        m_radarView.setLayerLineColor(Color.parseColor(COLOR_LAYER_LINE));
        m_radarView.setRotationEnable(false);
        m_radarView.setVertexText(Lst.of(MutiMsg.FORMULA_ACIDITY.msg(), MutiMsg.FORMULA_SWEET.msg(), MutiMsg.FORMULA_BODY.msg(), MutiMsg.FORMULA_AROMA.msg(), MutiMsg.FORMULA_AFTERTASTE.msg()).toList());
//        m_radarView.setVertexIconResid(Lst.of(R.mipmap.besticon, R.mipmap.besticon, R.mipmap.besticon, R.mipmap.besticon, R.mipmap.besticon).toList()); //這邊的 icon 只是為了讓  layout 正確，不加圖片的話字會跑版。
    }

    //----------------------------------------------------
    // public Method
    //----------------------------------------------------
    public void set(float acidity, float aftertaste, float sweet, float body, float aroma) {
        m_radarView.addData(new RadarData(Lst.of(acidity, sweet, body, aroma, aftertaste).toList())); // 需要按照順序: 酸度、甜度、香氣、醇厚、餘韻。
    }

    public void set(RadarModel m) {
        m.println_d(" radar_model_size");
        m_radarView.addData(m.toRadarData());
    }


    //----------------------------------------------------
    // Inner class
    //----------------------------------------------------
    public static class RadarModel {

        private float acidity = 0f;
        private float aftertaste = 0f;
        private float sweet = 0f;
        private float body = 0f;
        private float aroma = 0f;

        public static RadarModel create(float acidity, float aftertaste, float sweet, float body, float aroma) {
            RadarModel model = new RadarModel();
            model.setAcidity(acidity);
            model.setAftertaste(aftertaste);
            model.setSweet(sweet);
            model.setBody(body);
            model.setAroma(aroma);
            return model;
        }

        public RadarData toRadarData() {
            return new RadarData(Lst.of(getAcidity(), getSweet(), getBody(), getAroma(), getAftertaste()).toList());
        }

        public void println_d(String filter) {
            ArrayList<Pair<String, String>> datas = Lst.of(Pair.of("acidity", getAcidity() + ""), Pair.of("aftertaste", getAftertaste() + ""), Pair.of("sweet", getSweet() + ""), Pair.of("body", getBody() + ""), Pair.of("aroma", getAroma() + "")).toList();
            for (Pair<String, String> p : datas) {
                CsLog.d(RadarViewChart.class, filter.concat(p.k() + StrUtil.SPACE_COLON + p.v()));
            }
        }

        public boolean isEmpty() {
            for (float item : Lst.of(getAcidity(), getAftertaste(), getSweet(), getBody(), getAroma()).toList()) {
                if (item != 0.0)
                    return false;
            }
            return true;
        }

        //----------------------------------------------------
        // GETTER and SETTER
        //----------------------------------------------------
        public float getAcidity() {
            return acidity;
        }

        public void setAcidity(float acidity) {
            this.acidity = acidity;
        }

        public float getAftertaste() {
            return aftertaste;
        }

        public void setAftertaste(float aftertaste) {
            this.aftertaste = aftertaste;
        }

        public float getSweet() {
            return sweet;
        }

        public void setSweet(float sweet) {
            this.sweet = sweet;
        }

        public float getBody() {
            return body;
        }

        public void setBody(float body) {
            this.body = body;
        }

        public float getAroma() {
            return aroma;
        }

        public void setAroma(float aroma) {
            this.aroma = aroma;
        }
    }

}
