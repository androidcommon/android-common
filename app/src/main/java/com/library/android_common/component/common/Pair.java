package com.library.android_common.component.common;


import com.library.android_common.constant.CsLog;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.StrUtil;

import java.util.Objects;

/**
 * Created by Raymond Zorro on 18/04/2018.
 * 這裡是為了讓 Pair K, V 從 Object 轉 Generic Type
 */

public class Pair<K, V> {

    private K k;
    private V v;

    //-----------------------------------------------
    // construct
    //-----------------------------------------------
    public Pair() {
    }

    public Pair(K k, V v) {
        this.k = k;
        this.v = v;
    }

    static public <K, V> Pair<K, V> of(K k, V v) {
        return new Pair<>(k, v);
    }

    static public <K, V> Pair<K, V> splitOf(String middle, String content) {
        return new Pair<>((K) content.split(middle)[0], (V) content.split(middle)[1]);
    }

    //-----------------------------------------------
    // Data
    //-----------------------------------------------
    public void setK(K k) {
        this.k = k;
    }

    public void setV(V v) {
        this.v = v;
    }

    public K k() {
        return k;
    }

    public V v() {
        return v;
    }

    public Opt<K> optK() {
        return Opt.of(k);
    }

    public Opt<V> optV() {
        return Opt.of(v);
    }

    public Pair<K, V> swap() { // kv value 交換
        return Pair.of((K) v, (V) k);
    }

    public Pair<Opt<K>, Opt<V>> toOpt() {
        return Pair.of(Opt.of(k), Opt.of(v));
    }

    public Pair<Opt<K>, Opt<V>> optSwap() {
        return swap().toOpt();
    }

    public Lst<Pair<K, V>> toLst() {
        return Lst.of(Pair.of(k, v));
    }

    public Pairs<K, V> toPairs() {
        return Pairs.of(k, v);
    }

    //-----------------------------------------------
    // public method
    //-----------------------------------------------
    public String toString() {
        return "[" + k + ", " + v + "]";
    }

    public boolean compareTo(Pair<K, V> p) {
        return p.k().equals(k) && p.v().equals(v);
    }

    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (obj == null || !(obj instanceof Pair))
            return false;
        Pair<?, ?> other = (Pair<?, ?>) obj;
        return Objects.equals(k, other.k) &&
                Objects.equals(v, other.v);
    }

    public String addHead(String s) {
        return s + k + v;
    }

    public String addMiddle(String s) {
        return k + s + v;
    }

    public String addTail(String s) {
        return StrUtil.EMPTY + k + v + s;
    }

    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println() {
        JavaTools.println(toString());
    }

    public void println_d(String filter) {
        CsLog.d(Pair.class, filter.concat(toString()));
    }

}
