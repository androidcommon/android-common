package com.library.android_common.component.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.util.AttributeSet;
import android.widget.NumberPicker;

import com.library.android_common.component.view.ios_style.IOSTheme;

import java.lang.reflect.Field;

/**
 * Create By Ray on 2018/6/14
 * reference : https://stackoverflow.com/questions/24233556/changing-numberpicker-divider-color
 */
public class CNumberPicker extends NumberPicker {


    //--------------------------------------------------------------
    // Construct
    //--------------------------------------------------------------
    public CNumberPicker(Context context) {
        super(context);
        init();
    }

    public CNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public CNumberPicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    //--------------------------------------------------------------
    // Private Preference
    //--------------------------------------------------------------
    private void init() {
        setDividerColor(Color.parseColor(IOSTheme.Color.TEXT_BLUE.code()));
    }

    //--------------------------------------------------------------
    // Public Method
    //--------------------------------------------------------------
    public void setDividerColor(@ColorInt int color) {
        try {
            Field fDividerDrawable = NumberPicker.class.getDeclaredField("mSelectionDivider");
            fDividerDrawable.setAccessible(true);
            Drawable d = (Drawable) fDividerDrawable.get(this);
            d.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
            d.invalidateSelf();
            postInvalidate(); // Drawable is dirty
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
