package com.library.android_common.constant;

import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.library.android_common.util.StrUtil;

/**
 * Created by Raymond Zorro on 22/04/2018.
 */

public class CsLog {

    private static final String GLOBAL_TAG = "Samantha_Coffee_Machine";

    //--------------------------------------------------
    // Log
    public static void v(String content) {
        Log.v(GLOBAL_TAG, StrUtil.SPACE_COLON.concat(content));
    }

    public static void d(String content) {
        Log.d(GLOBAL_TAG, StrUtil.SPACE_COLON.concat(content));
    }

    public static void e(String content) {
        Log.e(GLOBAL_TAG, StrUtil.SPACE_COLON.concat(content));
    }

    public static void i(String content) {
        Log.i(GLOBAL_TAG, StrUtil.SPACE_COLON.concat(content));
    }

    public static void w(String content) {
        Log.w(GLOBAL_TAG, StrUtil.SPACE_COLON.concat(content));
    }

    //--------------------------------------------------

    public static void v(String content, Class<?> c) {
        Log.v(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void d(String content, Class<?> c) {
        Log.d(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void e(String content, Class<?> c) {
        Log.e(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void i(String content, Class<?> c) {
        Log.i(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void w(String content, Class<?> c) {
        Log.w(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void v(Class<?> c, String content) {
        Log.v(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void d(Class<?> c, String content) {
        Log.d(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void e(Class<?> c, String content) {
        Log.e(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void i(Class<?> c, String content) {
        Log.i(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    public static void w(Class<?> c, String content) {
        Log.w(GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
        Crashlytics.log(0, GLOBAL_TAG.concat(StrUtil.SPACE_DASH.concat(c.getSimpleName())), StrUtil.SPACE_COLON.concat(content));
    }

    //--------------------------------------------------


}
