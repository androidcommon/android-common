package com.library.android_common.constant;

/**
 * Created by Raymond Zorro on 19/04/2018.
 * Http Request 會用到的 Constant
 */

public class HttpCs {

    public static final String ID = "id";
    public static final String FB_ID = "fbid";
    public static final String FB_TOKEN = "fbtoken";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String APPLELANGUAGE = "AppleLanguage";
    public static final String APPLELANGUAGE_ZH_TW = "zh-TW";
    public static final String APPLELANGUAGE_ZH_HANT = "zh-Hant";
    public static final String ANDROID = "android";
    public static final String VERSION = "version";
    public static final String DEVICE = "device";
    public static final String SUCCESS = "success";
    public static final String TRUE = "true";
    public static final String FALSE = "false";
    public static final String API = "api";
    public static final String MESSAGE = "message";
    public static final String ERROR_CODE = "errcode";
    public static final String ERROR_MESSAGE = "errmsg";
    public static final String DETAIL = "detail";
    public static final String RESULT = "result";
    public static final String BIRTHDAY = "birthdat";
    public static final String INTRO = "intro";
    public static final String GENDER = "gender";
    public static final String STATUS = "status";
    public static final String NOTE = "note";
    public static final String TOKEN = "token";
    public static final String PHOTO = "photo";
    public static final String AVATAR = "avatar";
    public static final String KEYWORD = "keyword";
    public static final String WATER = "water";
    public static final String ORIGIN = "origin";
    public static final String PROCESS = "process";
    public static final String ROAST = "roast";
    public static final String FLAVOR = "flavor";
    public static final String APP_STATUS_IS_NEW_VERSION = "0";
    public static final String APP_STATUS_IS_OLD_VERSION = "1";
    public static final String ORIGIN_RECIPE_ID = "origin_recipe_id";
    public static final String DEV_API = "dev-";
    public static final String HTTPS = "https://";
    public static final String DATE_TIME_STAMP = "datetime";
    public static final String PUBLIC_RECIPE = "public";
    public static final String SEARCH_TYPE = "type";
    public static final String USER = "user";
    public static final String PAGE = "page";
    public static final String ACTION = "action";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String TYPE = "type";
    public static final String DATA = "data";
    public static final String BUTTON  = "button";

}
