package com.library.android_common.enums;

import android.util.Log;

import com.library.android_common.component.common.Lst;
import com.library.android_common.component.common.Pair;
import com.library.android_common.component.http.HttpFiles;
import com.library.android_common.component.http.HttpGets;
import com.library.android_common.component.http.HttpPosts;
import com.library.android_common.constant.CsLog;
import com.library.android_common.constant.HttpCs;
import com.library.android_common.util.StrUtil;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by Raymond Zorro on 19/04/2018.
 * 1. 用於呼叫 Samantha Api
 * 2. Api 需要將其定義在此
 * 3. 以定義好 Get 與 Post method
 * 4. 內部使用 Multiple Thread
 */

public enum HttpDef {


    //----------------------------------------------------------------
    // COMMON
    TEST("測試用", "http://headers.jsontest.com/"),
    TEST_PIC("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-449.jpg"),
    TEST_PIC_1("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-500.jpg"),
    TEST_PIC_2("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-501.jpg"),
    TEST_PIC_3("測試用的圖片", "https://image.freepik.com/free-photo/cute-cat-picture_1122-502.jpg"),;

    //--------------------------------------------------------------------------
    // 宣告
    //--------------------------------------------------------------------------
    private static final String POST_METHOD = "POST";
    private static final String GET_METHOD = "GET";

    private HttpDef m_Self = null;
    private String m_sDesc = StrUtil.EMPTY;
    private String m_sUrl = StrUtil.EMPTY;
    private String m_sCookie = StrUtil.EMPTY;
    private String m_sCLength = StrUtil.EMPTY;
    private String m_sCType = StrUtil.EMPTY;
    private String m_sConnType = StrUtil.EMPTY;
    private String m_sTag = StrUtil.EMPTY;

    private Map<String, String> m_Map = null;
    private ArrayList<?> m_Lst = null;
    private HttpDefResponse m_Res;
    private Class<?> m_Cls = HttpDef.class;
    private boolean m_isDevMode;

    //--------------------------------------------------------------------------
    // construct
    //--------------------------------------------------------------------------

    HttpDef(String sDesc, String sUrl) { // Basic
        m_sDesc = sDesc;
        m_sUrl = sUrl;
        m_Map = new LinkedHashMap<>();
        m_Self = this;
        dev(); // 直接在 init 呼叫 dev mode ，就不需要動到外部的 code

    }

    HttpDef(String sDesc, String sUrl, Class<?>... ca) { // 加上有使用到的 class
        m_sDesc = sDesc;
        m_sUrl = sUrl;
        m_Lst = Lst.of(ca).toList();
        m_Map = new LinkedHashMap<>();
        m_Self = this;
        dev(); // 直接在 init 呼叫 dev mode ，就不需要動到外部的 code
    }

    //--------------------------------------------------------------------------
    // public method
    //--------------------------------------------------------------------------
    public String getUrl() {
        return m_sUrl;
    }

    public String getsDesc() {
        return m_sDesc;
    }

    public ArrayList<?> getPairs() {
        return m_Lst;
    }

    public String getCookie() {
        return m_sCookie;
    }

    public String getCType() {
        return m_sCType;
    }

    public String getCLength() {
        return m_sCLength;
    }

    public String getTag() {
        return m_sTag;
    }

    //--------------------------------------------------------------------------
    // http connection
    //--------------------------------------------------------------------------
    public HttpDef post() {
        m_sConnType = POST_METHOD;
        return m_Self;
    }

    public HttpDef get() {
        m_sConnType = GET_METHOD;
        return m_Self;
    }

    public HttpDef addParam(String sKey, String sVal) { // post 添加參數
        m_Map.put(sKey, sVal);
        return m_Self;
    }

    public HttpDef dev() { // develop api

        if (m_isDevMode) return m_Self;
        //-----------------------------
        m_sUrl = HttpCs.HTTPS.concat(HttpCs.DEV_API)
                .concat(m_sUrl.substring(HttpCs.HTTPS.length(), m_sUrl.length()));
        m_isDevMode = true;

        return m_Self;
    }

    public HttpDef notDev() { // 其他外部請勿呼叫，這個方法只有，不含測試區的 API 需要呼叫，待全部移植正式區，要與 dev() 一起移除。 LOGIN, FB_LOGIN, REGISTER, FORGET_PASSWORD, CHANGE_PASSWORD, PLAY_STORE_SAMANTHA_URI
        m_sUrl = m_sUrl.replace(HttpCs.DEV_API, StrUtil.EMPTY);
        return m_Self;
    }

    //--------------------------------------------------------------------------
    // trigger
    //--------------------------------------------------------------------------
    public void println_d(Class<?>... cls) { // d 代表 log.d ， 輸入class 是 filter 的 key word
        m_Res = null;
        m_Cls = cls[0];
        //--------------------------

        launchConn();
    }

    public void trigger(HttpDefResponse response) {
        m_Res = response;
        //--------------------------
        launchConn();
    }

    public void trigger_f(List<Pair<String, String>> params, Pair<String, File> fileParam, HttpDefResponse response) { // f 代表 file loader
        m_Res = response;
        //--------------------------
        CsLog.d(HttpDef.class, StrUtil.bracketsOf(m_Self.name()) + " URL : ".concat(m_sUrl));

        try {

            Pair<String, Pair<List<Pair<String, String>>, Pair<String, File>>> ps =
                    Pair.of(m_Self.getUrl(), Pair.of(params, fileParam));

            HttpFiles httpUploader = new HttpFiles();
            Pair<String, JSONObject> pair = httpUploader.execute(ps).get();

            CsLog.d(HttpCs.class, StrUtil.bracketsOf(m_Self.name()) + " Http File Loader : ".concat(pair.k()));

            if (m_Res != null)
                m_Res.response(pair.k(), pair.v());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    //--------------------------------------------------------
    // private preferences
    //--------------------------------------------------------
    private void launchConn() {

        CsLog.d(HttpDef.class, StrUtil.bracketsOf(m_Self.name()) + " URL : ".concat(m_sUrl));
        checkStatus(); // 用以 check connection method
        //--------------------------------------------------------

        switch (m_sConnType) {
            case GET_METHOD:    // GET Method
                try {

                    HttpGets httpGet = new HttpGets();
                    Pair<String, JSONObject> pair = httpGet.execute(Pair.of("URL", m_Self.getUrl())).get();

                    Log.d(m_Cls.getName(), StrUtil.bracketsOf(m_Self.name()) + " SHOW GET Method " + " result : " + pair.k());

                    if (m_Res != null)
                        m_Res.response(pair.k(), pair.v());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;

            //--------------------------------------------------------
            case POST_METHOD:   // POST method
                try {

                    List<NameValuePair> lst = new ArrayList<>();
                    for (Map.Entry<String, String> map : m_Map.entrySet())
                        lst.add(new BasicNameValuePair(map.getKey(), map.getValue()));

                    HttpPosts httpPost = new HttpPosts();
                    Pair<String, JSONObject> pair = httpPost.execute(Pair.of(lst, m_Self.getUrl())).get();

                    Log.d(m_Cls.getName(), StrUtil.bracketsOf(m_Self.name()) + " SHOW POST Method " + " result : " + pair.k());
                    if (m_Res != null)
                        m_Res.response(pair.k(), pair.v());

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                break;

        } // switch

    }

    private void checkStatus() {    // 檢查 type 是否正確
        if (m_sConnType.isEmpty()) {
            Log.e(HttpDef.class.getName(), HttpDef.class.getName().concat("Connection Method Please set Get or Post"));
            return;
        }
    }

    //--------------------------------------------------------
    // Listener
    //--------------------------------------------------------
    public interface HttpDefResponse {
        void response(String response, JSONObject jsObj);
    }

}

