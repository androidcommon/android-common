package com.library.android_common.enums;

import com.library.android_common.component.common.Lst;
import com.library.android_common.constant.CsLog;
import com.library.android_common.database.sp.SpCurrentLanguage;
import com.library.android_common.file.FileUtil;
import com.library.android_common.file.Pub;
import com.library.android_common.file.Row;
import com.library.android_common.file.Rows;
import com.library.android_common.util.JavaTools;

import java.util.Locale;

/**
 * Created by Raymond Zorro on 03/05/2018.
 * 用以record 不同國家的 language.
 * <p>
 * 參考文獻 https://likfe.com/2017/05/10/android-sys-language/
 */

public enum MutiMsg {

    //=====================================================================================
    // =start=
    //=====================================================================================

    CURRENT_LANGUAGE("zh-tw", "en-us", "zh-cn", "ja-jp", "th-th"), // 取目前的語系
    SETTING_PRIVACY("隱私政策", "Privacy Policy", "隐私政策", "プライバシーポリシー", "นโยบายความเป็นส่วนตัว"),
    SETTING_SERVICE("使用條款", "Terms of Service", "使用条款", "利用規約", "ข้อกำหนดในการให้บริการ"),
    LANGUAGE_SELECT("選擇", "select", "选择", "選択", "เลือก"),

    LANGUAGE("語言", "Language", "语言", "言語", "ภาษา"),
    LANGUAGE_JP("日語", "Japanese", "日语", "日本語", "ภาษาญี่ปุ่น"),
    LANGUAGE_CH("繁體中文", "Traditional Chinese", "繁体中文", "繁体字中国語", "จีนดั้งเดิม"),
    LANGUAGE_SC("簡體中文", "Simplified Chinese", "简体中文", "繁体字中国語", "จีนตัวย่อ"),
    LANGUAGE_EG("英語", "English", "英语", "英語", "ภาษาอังกฤษ"),

    CONNECTION_LESS("網路未連線", "network not connected", "网路未连线", "フライン", "อินเทอร์เน็ตออฟไลน์"),


    CLOUD_PERSONAL_EMAIL_INPUT_IS_EMPTY("請輸入電子郵件", "please input email", "请输入电子邮件", "メールを入力してください", "โปรดป้อนอีเมล"),
    CLOUD_BREW("沖煮", "Brew", "沖煮", "抽出", "การต้มเบียร์"),
    CLOUD_TITLE("無資料", "No data", "无资料", "データなし", "ไม่มีข้อมูล"),
    SPEED_LV("流速", "speed", "流速", "ベロシティ", "อัตราการไหล"),
    INTERVAL("間隔", "interval", "间隔", "間隔", "ระยะห่าง"),

    //-----------------------------------------------------
    //-----通用＆常用-----
    //-----------------------------------------------------
    OK("確認", "OK", "确认", "確認", "ตกลง"),
    CANCEL("取消", "Cancel", "取消", "キャンセル", "ยกเลิก"),
    DEVICE_NAME("Samantha", "Samantha", "Samantha", "Samantha", "ซาแมนต้า"),
    PAUSE("暫停", "Pause", "暂停", "ポーズ", "หยุด"),
    EDIT("編輯", "Edit", "编辑", "編修", "แก้ไข"),
    SKIP("略過", "Skip", "略过", "スキップ", "กระโดด"),
    MALE("男性", "Male", "男性", "男性", "ชาย"),
    FEMALE("女性", "Female", "女性", "女性", "เพศหญิง"),
    GENDER("性別", "Gender", "性别", "セックス", "เพศ"),
    WEIGHT("粉重", "weight", "粉重", "重さ", "นำ้หนัก"),
    HEATING_STOPPED("加熱動作停止", "Heating stopped", "加热动作停止", "加熱中止", "หยุดการทำงานของเครื่องทำความร้อน"),
    CHECK_REMAINING_WATER_VOLUME("請檢查水箱水量是否足夠", "Please check the remaining water volume.", "请检查水箱水量是否足够", "水槽残りの水量を確認してください。", "โปรดตรวจสอบว่าน้ำในถังเพียงพอหรือไม่"),

    CLOUD_SEARCH_FORMULA_TEXT_ORIGIN("產地", "origin", "产地", "原点", "สถานที่ต้นกำเนิด"),
    GRINDING_DEGREE("研磨度", "grinding degree", "研磨度", "粗さ設定", "ระดับเจียร"),
    CLOUD_SEARCH_FORMULA_TEXT_WATER("水量", "water", "水量", "水量", "ปริมาณน้ำ"),
    CLOUD_SEARCH_FORMULA_TEXT_FAVOR("風味", "flavor", "风味", "フレーバー", "รสชาติ"),
    CLOUD_BROWSE_FORMULA_TEXT_BROWSE("瀏覽", "Browse", "浏览", "ブラウジング", "หมวด"),
    CLOUD_BROWSE_FORMULA_TEXT_FORMULA("配方", "Formula", "配方", "レシピ", "สูตร"),
    CLOUD_BROWSE_FORMULA_TEXT_APPARATUS("裝置", "Apparatus", "装置", "装置", "อุปกรณ์"),
    CLOUD_BROWSE_FORMULA_TEXT_PERSONAL("個人", "Personal", "个人", "個人", "ส่วนบุคคล"),
    MY_RECIPE("我的配方", "My Recipe", "我的配方", "マイレシピ", "เมนูของฉัน"),
    FORMULA_ALREADY_DELETE_FAIL("配方刪除失敗", "fail !!", "配方删除失败", "レシピの削除に失敗しました", "การลบสูตรล้มเหลว"),
    BOOKMARK_RECIPE("收藏成功", "successful", "收藏成功", "ブックマーク完了", "ความสำเร็จของคอลเล็กชัน"),
    BOOKMARK_RECIPE_DELETE("取消收藏成功", "successful", "取消收藏成功", "ブックマークキャンセル完了", "ไม่ยอมรับความสำเร็จ"),
    BOOKMARK_RECIPE_FAIL("收藏失敗", "fail !!", "收藏失敗", "ブックマーク失敗", "ไม่สามารถรวบรวม"),
    BOOKMARK_RECIPE_FAIL_DELETE("取消收藏失敗", "fail !!", "取消收藏失敗", "ブックマークキャンセル失敗", "Uncollection ล้มเหลว"),
    DOWNLOAD_RECIPE("下載配方成功", "download recipe successful", "下载配方成功", "レシピダウンロード完了", "ดาวน์โหลดสูตรสำเร็จ"),
    DOWNLOAD_RECIPE_FAIL("下載配方失敗", "download recipe fail !!", "下载配方失败", "レシピダウンロード失敗", "ดาวน์โหลดสูตรล้มเหลว"),
    CANCEL_BOOKMARK("取消收藏", "cancel bookmark", "取消收藏", "ブックマークキャンセル", "รายการโปรด"),
    CLOUD_DOWNLOAD_FORMULA("下載配方", "download formula", "下载配方", "フォーミュラダウンロード", "ดาวน์โหลดสูตร"),
    CLOUD_DOWNLOAD_FORMULA_SWITCH("是否立即前往沖煮配方?", "brew recipe immediately?", "是否立即前往沖煮配方?", "今すぐ入れることにしますか？", "ไม่ว่าจะไปสูตรชงทันทีหรือไม่"),
    RECIPE_CANCEL_BOOK_TITLE("取消收藏配方", "Turn BookMark Off", "取消收藏配方", "レシピ収集をキャンセルする", "ยกเลิกสูตรการรวบรวม"),
    RECIPE_CANCEL_SHARE("是否取消分享該筆配方?", "cancel the recipe sharing?", "是否取消分享该笔配方?", "レシピシェアをキャンセルするか？", "ไม่ว่าจะยกเลิกการแบ่งปันสูตรหรือไม่"),
    RECIPE_CANCEL_SHARE_TITLE("取消分享配方", "Turn Sharing Off", "取消分享配方", "レシピシェアをキャンセルする", "ยกเลิกสูตรแบ่งปัน"),


    COUNTRY_MIX("特調", "Special adjustment", "特调", "ミックス", "การปรับพิเศษ"),
    COUNTRY_HAW("夏威夷", "Hawaii", "夏威夷", "ハワイ", "ฮาวาย"),
    COUNTRY_MX("墨西哥", "Mexico", "墨西哥", "メキシコ", "เม็กซิโก"),
    COUNTRY_GT("瓜地馬拉", "Guatemala", "瓜地马拉", "グアテマラ", "กัวเตมาลา"),
    COUNTRY_SV("薩爾瓦多", "El Salvador", "萨尔瓦多", "エルサルバドル", "เอลซัลวาดอร์"),
    COUNTRY_HN("宏都拉斯", "Honduras", "宏都拉斯", "ホンジュラス", "ฮอนดูรัส"),
    COUNTRY_NI("尼加拉瓜", "Nicaragua", "尼加拉瓜", "ニカラグア", "นิการากัว"),
    COUNTRY_CR("哥斯大黎加", "Costa Rica", "哥斯大黎加", "コスタリカ", "อสตาริกา"),
    COUNTRY_PA("巴拿馬", "Panama", "巴拿马", "パナマ", "ปานามา"),
    COUNTRY_EC("厄瓜多", "Ecuador", "厄瓜多", "エクアドル", "เอกวาดอร์"),
    COUNTRY_DO("多明尼加共和國", "Dominican Republic", "多明尼加共和国", "ドミニカ共和国", "สาธารณรัฐโดมินิกัน"),
    COUNTRY_CU("古巴", "Cuba", "古巴", "キューバ", "คิวบา"),
    COUNTRY_JM("牙買加", "Jamaica", "牙买加", "ジャマイカ", "เกาะจาเมกา"),
    COUNTRY_VE("委內瑞拉", "Venezuela", "委內瑞拉", "ベネズエラ", "เวเนซุเอลา"),
    COUNTRY_CO("哥倫比亞", "Colombia", "哥伦比亚", "コロンビア", "โคลอมเบีย"),
    COUNTRY_PE("秘魯", "Peru", "秘鲁", "ペルー", "เปรู"),
    COUNTRY_PR("波多黎各", "Puerto Rico", "波多黎各", "プエルトリコ", "เปอร์โตริโก"),
    COUNTRY_PY("巴拉圭", "Paraguay", "巴拉圭", "パラグアイ", "ประเทศแพรากเว"),
    COUNTRY_AR("阿根廷", "Argentina", "阿根廷", "アルゼンチン", "อาร์เจนตินา"),
    COUNTRY_CD("剛果（金夏沙）", "Congo (Kinshasha)", "刚果（金夏沙）", "コンゴ民主共和国（キンシャサ）", "คองโก (คินชาชา)"),
    COUNTRY_CF("中非共和國", "Central African Republic", "中非共和国", "中央アフリカ共和国", "สาธารณรัฐแอฟริกากลาง"),
    COUNTRY_IN("印度", "India", "印度", "インド", "อินเดีย"),
    COUNTRY_TH("泰國", "Thailand", "泰国", "タイ", "ประเทศไทย"),
    COUNTRY_KE("肯亞", "Kenya", "肯亚", "ケニア", "ประเทศเคนย่า"),
    COUNTRY_CN("中國", "China", "中国", "中国", "ประเทศจีน"),
    COUNTRY_KH("柬埔寨", "Cambodia", "柬埔寨", "カンボジア", "ประเทศกัมพูชา"),
    COUNTRY_TW("台灣", "Taiwan", "台湾", "台湾", "ไต้หวัน"),
    COUNTRY_VN("越南", "Vietnam", "越南", "ベトナム", "เวียดนาม"),
    COUNTRY_PH("菲律賓", "Philippines", "菲律宾", "フィリピン", "ฟิลิปปินส์"),
    COUNTRY_MY("馬來西亞", "Malaysia", "马来西亚", "マレーシア", "ประเทศมาเลเซีย"),
    COUNTRY_v("巴布亞紐幾內亞", "Papua New Guinea", "巴布亚纽几内亚", "パプアニューギニア", "ปาปัวนิวกีนี"),
    COUNTRY_BR("巴西", "Brazil", "巴西", "ブラジル", "บราซิล"),
    COUNTRY_ID("印尼", "Indonesia", "印尼", "インドネシア", "อินโดนีเซีย"),
    COUNTRY_ET("衣索比亞", "Ethiopia", "衣索比亚", "エチオピア", "สาธารณรัฐเอธิโอเปีย"),
    COUNTRY_TZ("坦尚尼亞", "Tanzania", "坦尚尼亚", "タンザニア", "ประเทศแทนซาเนีย"),
    COUNTRY_RW("盧安達", "Luanda", "卢安达", "ルワンダ", "รวันดา"),
    COUNTRY_YE("葉門", "Yemen", "叶门", "イエメン", "เยเมน"),

    WASHED("水洗", "Washed", "水洗", "洗濯", "การซัก"),
    SEMI_WASHED("半水洗", "Semi-washed", "半水洗", "半洗", "กึ่งล้าง"),
    DRY("日曬", "Dry", "日晒", "日焼け", "แสงแดด"),
    HONEY_HANDEL("蜜處理", "Honey", "蜜处理", "ハニー加工", "การประมวลผลน้ำผึ้ง"),
    WET_HULLED("濕剝", "Wet hulled", "湿剥", "ウェット剥離", "การปอกเปียก"),
    WEATHER_STAINS("風漬", "Weather stains", "风渍", "天気の汚れ", "คราบอากาศ"),

    LIGHT_ROAST("淺焙", "Light Roast", "浅焙", "浅いベーキング", "อบตื้น"),
    CINNAMON_ROAST("肉桂焙", "Cinnamon Roast", "肉桂焙", "シナモンベーキング", "อบเชยอบเชย"),
    MEDIUM_ROAST("中焙", "Medium Roast", "中焙", "焼く", "ในการอบ"),
    HIGH_ROAST("深焙", "High Roast", "深焙", "ディープベーキング", "การอบลึก"),
    FULL_CITY_ROAST("城市深焙", "Full City Roast", "城市深焙", "アーバンディープベーキング", "เมืองอบลึก"),
    DEEP_CITY_BAKING("深城市烘焙", "Deep City Baking", "深城市烘焙", "ディープシティベーキング", "การทำอาหารเมืองลึก"),
    FRENCH_ROAST("法式烘焙", "French Roast", "法式烘焙", "フレンチベーキング", "การอบฝรั่งเศส"),
    ITALIAN_ROAST("義式烘焙", "Italian Roast", "义式烘焙", "イタリアンベーキング", "การอบอิตาลี"),

    ALMOND("杏仁", "almond", "杏仁", "アーモンド", "อัลมอนด์"),
    APPLE("蘋果", "apple", "苹果", "リンゴ", "แอปเปิล"),
    APRICOT_PEACH("杏桃", "apricot_peach", "杏桃", "アプリコットピーチ", "ลูกพีชแอปปริคอท"),
    AVOCADO("酪梨", "avocado", "酪梨", "アボカド", "อะโวคาโด"),
    BLACK_TEA("紅茶", "black_tea", "紅茶", "紅茶", "ชาดำ"),
    BLUEBERRY("藍莓", "blueberry", "蓝莓", "ブルーベリー", "บลูเบอร์รี่"),
    CANTALOUPE("哈密瓜", "cantaloupe", "哈密瓜", "蜜メロン", "แตงโมแตงโม"),
    CARAMEL("焦糖", "caramel", "焦糖", "キャラメル", "กาละแม"),
    CHERRY("櫻桃", "cherry", "樱桃", "チェリー", "เชอร์รี่"),
    CHOCOLATE("巧克力", "chocolate", "巧克力", "チョコレート", "ช็อคโกแลต"),
    CINNAMON("肉桂", "cinnamon", "肉桂", "シナモン", "อบเชย"),
    CREAM("奶油", "cream", "奶油", "クリーム", "ครีม"),
    DRAGON_FRUIT("火龍果", "dragon_fruit", "火龙果", "ピタヤ", "พิทยา"),
    FLOWER("花香", "flower", "花香", "花", "บุหงา"),
    GRAPE("葡萄", "grape", "葡萄", "ブドウ", "องุ่น"),
    GRAPEFRUIT("柚子", "grapefruit", "柚子", "グレープフルーツ", "ส้มโอ"),
    HAZELNUT("榛果", "hazelnut", "榛果", "ヘーゼルナッツ", "เฮเซลนัท"),
    HONEY("蜂蜜", "honey", "蜂蜜", "ハニー", "น้ำผึ้ง"),
    KIWI("奇異果", "kiwi", "奇异果", "キウイフルーツ", "ผลไม้กีวี"),
    LEMON("檸檬", "lemon", "柠檬", "レモン", "มะนาว"),
    LIME("萊姆", "lime", "莱姆", "ライム", "Lyme"),
    LITCHI("荔枝", "litchi", "荔枝", "リッチー", "ลิ้นจี่"),
    LONGAN("龍眼", "longan", "龙眼", "竜眼", "ลำไย"),
    MALT("麥芽", "malt", "麦芽", "モルト", "ข้าวมอลต์"),
    MANGO("芒果", "mango", "芒果", "マンゴー", "มะม่วง"),
    MAPLE_SUGAR("楓糖", "maple_sugar", "枫糖", "メープルシュガー", "น้ำตาลเมเปิ้ล"),
    MILK("牛奶", "milk", "牛奶", "ミルク", "นม"),
    MINT("薄荷", "mint", "薄荷", "ペパーミント", "ทำเหรียญ"),
    ORANGE("柳橙", "orange", "柳橙", "オレンジ", "สีส้ม"),
    PAPAYA("木瓜", "papaya", "木瓜", "パパイヤ", "มะละกอ"),
    PEACH("桃子", "peach", "桃子", "ピーチ", "พีช"),
    PEAR("梨子", "pear", "梨子", "洋ナシ", "แพร์"),
    PINEAPPLE("鳳梨", "pineapple", "凤梨", "パイナップル", "สับปะรด"),
    RED_WINE("紅酒", "red_wine", "紅酒", "赤ワイン", "ไวน์แดง"),
    ROASTED("碳培", "roasted", "碳培", "炭素文化", "วัฒนธรรมคาร์บอน"),
    SPICES("辛香料", "spices", "辛香料", "スパイス", "เครื่องเทศ"),
    STRAWBERRY("草莓", "strawberry", "草莓", "イチゴ", "สตรอเบอร์รี่"),
    SUGAR_CANE("甘蔗", "sugar_cane", "甘蔗", "サトウキビ", "อ้อย"),
    SWEET_POTATO("番薯", "sweet potato", "番薯", "サツマイモ", "มันฝรั่งหวาน"),
    VANILLA("香草", "vanilla", "香草", "バニラ", "วานิลลา"),
    WALNUT("核桃", "walnut", "核桃", "ウォールナッツ", "ต้นมันฮ่อ"),
    WATERMELON("西瓜", "watermelon", "西瓜", "スイカ", "แตงโม"),
    WHISKEY("威士忌", "whiskey", "威士忌", "ウィスキー", "วิสกี้"),


    //-----bluetooth page-----
    CONNECT_TO_SAMANTHA("連接咖啡機...", "Connect to samantha...", "連接咖啡機", "コーヒーマシンを接続する", "ติดต่อกับ samantha…"),
    BLE_LIST_TITLE("搜尋到的咖啡機", "machine found list", "搜寻到的咖啡机", "コーヒーマシンリスト", "เครื่องชงกาแฟพบ"),

    //-----welcome page-----
    DATA_INIT("初始化資料中...", "Initializing...", "初始化资料中...", "資料初期化中", "ในข้อมูลการเริ่มต้น"),
    WELCOME("歡迎使用", "WELCOME", "欢迎使用", "ようこそ", "ยินดีต้อนรับ"),
    LOGIN_NEEDED("請先登入帳號以獲得更佳的使用服務", "Please login for full features", "透过点击继续，表示您同意使用条款及隐私权政策", "「次へ」のボタンをクリックし、利用規約とプライバシーポリシーに同意します。", "โปรดเข้าสู่บัญชีของคุณเพื่อขอรับบริการที่ดียิ่งขึ้น"),
    LOGIN("登入使用", "login", "登入使用", "ログイン", "ล็อกอิน"),
    CREATE_ACCOUNT("建立帳號", "create account", "建立账号", "新しいアカウントを作成", "สร้างแอคเค้าต์"),
    MORE_CONTENT("查看內容", "more content", "查看内容", "更なるコンテンツ", "ข้อมูลเพิ่มเติม"),
    WELCOME_POLICY_TEXT("點擊以上表示您同意我們的使用條款及隱私權政策", "By clicking above, you agree to our Terms of Use and Privacy Policy", "点击以上表示您同意我们的使用条款及隐私权政策", "上記をクリックすると、利用規約とプライバシーポリシーに同意したことになります", "เมื่อคลิกด้านบนแสดงว่าคุณยอมรับข้อกำหนดในการให้บริการและนโยบายความเป็นส่วนตัวของเรา"),

    DEVICE_BLE_NOT_SUPPORT("偵測到本裝置不支援藍芽BLE", "Not Support BLE Device", "侦测到本装置不支援蓝芽BLE", "ブルートゥース BLE はサポートされない。", "ไม่พบอุปกรณ์นี้เพื่อสนับสนุน Bluetooth BLE"),
    DEVICE_BLE_IS_TURN_OFF("偵測到藍芽裝置未開啟", "Bluetooth is turn off", "侦测到蓝芽装置未开启", "ブルートゥース機能オフ", "ไม่พบอุปกรณ์บลูทู ธ"),

    //-----login page-----
    FACEBOOK_LOGIN("FACEBOOK登入", "Facebook Login", "FB登入", "フェイスブックログイン", "ล็อคอิน เฟซบุ๊ค"),
    YOUR_EMAIL("請填入電子信箱", "Your email", "请填入电子信箱", "メールアドレスをご記入ください。", "อีเมล"),
    PASSWORD("請填入密碼", "Password", "请填入密码", "パスワードの入力", "รหัสผ่าน"),
    LOGIN_TITLE("登入", "Login", "登入", "ログイン", "เข้าสู่ระบบ"),
    LOGIN_NOW("登入中...", "Login...", "登入中...", "ログイン中", "การลงนามใน"),
    LOGIN_SUCCESS("登入成功", "Login Success", "登入成功", "ログイン成功", "เข้าสู่ระบบสำเร็จแล้ว"),
    LOGIN_FAIL("登入失敗，帳號或密碼錯誤", "Login Fail", "登入失败，帐号或密码错误", "ログインエラー", "การเข้าสู่ระบบล้มเหลวด้วยบัญชีหรือรหัสผ่านไม่ถูกต้อง"),
    LOGIN_FAIL_NORMAL("登入失敗", "Login Fail", "登入失敗", "ログインエラー", "เข้าสู่ระบบล้มเหลว"),

    //-----create account page-----
    CREATE_ACCOUNT_TEXT("歡迎成為我們的一份子,一起來享受咖啡的美好吧", "Welcome to be a part of us and enjoy the coffee together", "欢迎成为我们的一份子,一起来享受咖啡的美好吧", "ようこそ私たちの一部であり、一緒にコーヒーをお楽しみください", "ยินดีต้อนรับสู่การเป็นส่วนหนึ่งของเราและเพลิดเพลินไปกับกาแฟด้วยกัน"),
    CREATE_ACCOUNT_PASSWORD("密碼", "Password", "密码", "パスワード", "รหัสผ่าน"),
    CREATE_ACCOUNT_CONFIRM_PASSWORD("確認密碼", "Confirm Password", "确认密码", "パスワードを認証する", "ยืนยันรหัสผ่าน"),
    CREATE_ACCOUNT_BTN("完成送出", "Send", "完成送出", "転送", "ส่งมอบ"),

    REGISTING("帳號註冊中...", "creating account", "帐号注册中...", "アカウントを新設中", "การลงทะเบียนบัญชี"),
    THIRD_GENDER("其他", "Third Gender", "其他", "その他", "อื่น ๆ"),

    //-----咖啡雲 搜尋配方-----
    CLOUD_SEARCH_FORMULA_TITLE("搜尋篩選", "Search filter", "搜寻筛选", "検索フィルタ", "ตัวกรองการค้นหา"),
    CLOUD_SEARCH_FORMULA_RESET("重設", "Reset", "重设", "リセット", "รีเซ็ต"),
    CLOUD_SEARCH_FORMULA_EDIT("搜尋配方", "Search Recipe", "搜寻配方", "検索式", "สูตรการค้นหา"),
    CLOUD_SEARCH_FORMULA_TEXT_PROCESS("處理法", "Processing", "处理法", "処理方法", "วิธีการประมวลผล"),
    CLOUD_SEARCH_FORMULA_TEXT_BAKING("烘培", "Baking", "烘培", "焼く", "การอบ"),
    CLOUD_SEARCH_FORMULA_TEXT_ITEM("未選擇", "Not selected", "未选择", "選択されていない", "ไม่ได้เลือก"),
    CLOUD_SEARCH_FORMULA_BTN("進行篩選", "Filtering", "筛选", "スクリーニング", "กรอง"),

    //-----咖啡雲 瀏覽頁面-----
    CLOUD_BROWSE_FORMULA_EDIT("找配方", "Find formula", "找配方", "レシピを探す", "หาสูตร"),
    CLOUD_BROWSE_FORMULA_TAB_POPULAR("熱門", "Hot", "热门", "ホット", "ร้อน"),
    CLOUD_BROWSE_FORMULA_TAB_MASTER("大師", "Master", "大师", "マスター", "เจ้านาย"),
    CLOUD_BROWSE_FORMULA_TAB_NEWS("最近", "News", "最近", "最近", "เมื่อเร็ว ๆ นี้"),

    PRODUCT("產品", "Product", "产品", "製品", "สินค้า"),
    MAC_ADDRESS("序號", "Number", "序号", "いいえ", "เลขที่"),
    REGISTER("註冊時間", "Register Time", "注册时间", "登録時間", "เวลาลงทะเบียน"),

    RECIPE_BREW_NOW("讀取配方中...", "reading recipe", "读取配方中...", "レシピを受けています", "อ่านสูตร"),
    RECIPE_BREW_FAIL("讀取配方失敗", "reading recipe fail", "读取配方失败", "レシピを受けるエラー", "อ่านสูตรไม่สำเร็จ"),
    SWITCH_SLOWLY("請勿過於頻繁切換", "switch tab slowly", "请勿过于频繁切换", "タブをゆっくり変更してください", "อย่าเปลี่ยนบ่อยเกินไป"),
    CLOUD_SEARCH_MSG("沒有相關資訊", "No relevant information", "没有相关资讯", "相応情報がありません", "ไม่มีข้อมูลที่เกี่ยวข้อง"),

    //-----咖啡雲 配方詳細資料-----
    CLOUD_DETAILED_FORMULA_TEXT("分享者:", "Sharer:", "分享者", "シェアラー:", "คนที่แชร์:"),

    //-----咖啡雲 配方頁面-----
    SHARE_RECIPE_TITLE("分享配方", "Share Recipe", "分享配方", "レシピシェア", "แชร์สูตร"),
    SHARE_RECIPE("確定分享配方?", "public your recipe ?", "確定分享配方?", "レシピシェアしますか？", "โปรดแชร์สูตร"),
    SHARE_CANCEL_PUBLIC("取消分享成功", "cancel public", "取消分享成功", "レシピシェアをキャンセルしました", "การแชร์ไม่สำเร็จ"),
    SHARE_SUCCESSED("分享成功", "successful", "分享成功", "レシピシェアしました", "แบ่งปันความสำเร็จ"),
    SHARE_FAIL("分享失敗", "fail", "分享失败", "レシピシェアにエラーがあります", "การแบ่งปันล้มเหลว"),
    SHARE_CANCEL_FAIL("取消分享失敗", "fail", "取消分享失败", "レシピシェアのキャンセルにエラーがあります", "ยกเลิกการแชร์ล้มเหลว"),
    MY_COLLECTION("我的收藏", "My Collection", "我的收藏", "マイコレクション", "ของสะสมของฉัน"),
    MY_COLLECTION2("我的收藏", "My Collection", "我的收藏", "マイコレクション", "ของสะสมของฉัน"),


    //-----關閉App Dialog-----
    CLOSE_APP("關閉程式", "Close program", "关闭程式", "アプリを終わります", "ปิดโปรแกรม"),
    CLOSE_SAMANTHA_APP("是否離開本程式?", "Whether to leave this program?", "是否离开本程式?", "アプリを終わりますか？", "ไม่ว่าจะออกจากโปรแกรมนี้หรือไม่？"),

    //-----ProgressDialog Msg-----
    DEVICE_PROGRESS("讀取中, 請稍候", "During reading, please wait", "读取中, 请稍候", "読んでいる間、お待ちください", "ระหว่างการอ่านโปรดรอสักครู่"),

    //-----samantha page-----
    CLOUD_COFFEE("咖啡雲", "Cloud", "咖啡云", "クラウド", "คลาวด์"),
    LOGIN_REQUEST("此服務需要登入方可使用，是否前往登入?", "go to login page?", "此服务需要登入方可使用，是否前往登入?", "ログインに変更しますか？", "บริการนี้ต้องเข้าสู่ระบบเพื่อใช้คุณจะเข้าสู่ระบบหรือไม่?"),
    LOGIN_ALERT("未登入帳號將會限制部分使用功能", "To use coffee cloud need to login", "未登入帐号将会限制部分使用功能", "ログインしないと、ある機能が出来ないことがあります。", "การไม่ลงชื่อเข้าใช้บัญชีจะ จำกัด คุณลักษณะบางอย่างไว้"),

    //-----設定 page-----
    SETTING("設定", "Setting", "设定", "設定", "ตั้งค่า"),
    MACHINE_SETTING("咖啡機設置", "Machine setting", "咖啡机设置", "Samantha設定", "การตั้งค่าเครื่อง"),
    SELECT_MACHINE("選擇咖啡機", "Select machine", "选择咖啡机", "マシンをご選択ください。", "เลือกอุปกรณ์"),
    CLEAN_MODE("清潔模式", "Clean mode", "清洁模式", "洗浄モード", "โหมดทำความสะอาด"),
    RESET_DEFAULT_SETTING("回復原廠設定", "Reset default setting", "回复原厂设定", "デフォルト設定", "รีเซ็ทค่าตั้งต้น"),
    HELP("協助", "Help", "协助", "ヘルプ", "ความช่วยเหลือ"),
    FEEDBACK("意見回饋", "Feedback", "意见回馈", "フィードバック", "ข้อเสนอแนะ"),
    ABOUT("關於", "About", "关于", "Samanthaについて", "เกี่ยวกับเรา"),

    //-----設定 選擇咖啡機-----
    AUTO_CONNECT("自動連線", "Auto-connect", "自动联机", "自動接続", "ติดต่ออัตโนมัติ"),
    COFFEE_MACHINE_NEARBY("附近的咖啡機", "Coffee machine nearby", "附近的咖啡机", "コーヒーマシン近く", "เครื่องชงกาแฟใกล้เคียง"),


    DEVICE_CONNECTING("_連線中", "connect to_", "_连线中", "_に接続中", "_ในการเชื่อมต่อ"), // _ 用以 replace or split
    DEVICE_SCANNINIG("Samantha 掃描中...", "scanning...", "Samantha 扫描中", "ディバイス捜索中", "การสแกนซาแมนต้า"),
    DEVICE_BLE_DISCONNECT_TITLE("中斷連線", "disconnect", "中断连线", "接続切断", "การเชื่อมต่อที่ขัดจังหวะ"),
    DEVICE_BLE_DISCONNECT_MSG("確定要中斷與_的連結？", "disconnect with _", "确定要中断与_的连结？", "_との接続を切りますか？", "คุณแน่ใจหรือไม่ว่าต้องการเลิกลิงก์ด้วย _?"), // _ 用以 replace or split

    //-----設定 咖啡機設定-----
    COFFEE_MACHINE_SETTINGS("咖啡機設定", "Coffee machine settings", "咖啡机设定", "コーヒーマシンの設定", "การตั้งค่าเครื่องชงกาแฟ"),
    OPEN_PASSWORD_LOCK("開啟密碼鎖", "Open password lock", "开启密码锁", "オープンパスワードロック", "เปิดล็อกรหัสผ่าน"),
    EDIT_NAME("編輯名稱", "Edit name", "编辑名称", "名前を編集", "แก้ไขชื่อ"),
    EDIT_PASSWORD("編輯密碼", "Edit password", "编辑密码", "パスワードの編集", "แก้ไขรหัสผ่าน"),
    EDIT_NAME_MSG("字數過長，請重新輸入(英文12字元，中文4字元)", "The number of characters is too long. Please re-enter (English 12 characters, Chinese 4 characters)", "字数过长，请重新输入(英文12字元，中文4字元)", "文字数が長すぎます。再入力してください（英語12文字、中国語4文字）", "จำนวนตัวอักษรยาวเกินไปโปรดป้อนใหม่ (ภาษาอังกฤษ 12 ตัวอักษรภาษาจีน 4 ตัว)"),

    //-----samantha 排水階段-----
    WARNING("警告", "warning", "警告", "警告", "การเตือน"),
    DRAIN("機器排水", "Drain", "机器排水", "水の排出", "ระบบนำ้ทิ้ง"),
    WATER_OUTLET("出水孔下方請放置承接水容器", "Place water container under water outlet", "出水孔下方请放置承接水容器", "水容器を水出口の下に置きます", "วางภาชนะบรรจุน้ำใต้เต้าเสียบน้ำ"),
    READY_TO_DRIP("好的，直接排水", "OK do it", "好的，直接排水", "確認、排水をします", "ดีการระบายน้ำโดยตรง"),
    DRAINING("排水中...", "Draining...", "排水中...", "水の流出…", "กำลังระบายนำ้ทิ้ง"),
    STOP_DRAINAGE("停止排水", "Stop", "停止", "排水を止める", "หยุดการระบายน้ำ"),

    //-----登入 忘記密碼-----
    FORGOT_PASSWORD("忘記密碼？", "forget password?", "忘记密码？", "パスワードを忘れた場合", "ลืมรหัสผ่าน?"),
    INPUT_HIROIA_ACCOUNT("請填入申請HIROIA帳號\n時使用的電子信箱 我們\n將會寄送一組新密碼給您", "Enter the email address \nassociated with your \naccount a new password \nwill be sent to you", "请填入申请HIROIA账号\n时使用的电子信箱 我们\n将会寄送一组新密码给您", "HIROIAアカウント\n開設時に使用したメールアドレスをご入力ください。新規パスワードのメールを\n送信します。", "โปรดกรอกใบสมัครสำหรับบัญชี HIROIA\n อีเมลที่เราใช้\n รหัสผ่านใหม่จะถูกส่งถึงคุณ"),
    EMAIL("電子信箱", "E-mail", "电子信箱", "メールアドレス", "อีเมล"),
    RESET_PASSWORD("重置密碼", "Reset password", "重置密码", "パスワードをリセットします。", "รีเซ็ทรหัสผ่าน"),

    UPDATE_PASSWORD_NOW("密碼修改中...", "update password", "密码修改中...", "パスワード変更中", "การแก้ไขรหัสผ่าน"),

    //-----咖啡雲 搜尋配方&沖煮筆記Item-----
    MAX_FLAVORS_ITEM("最多可選４項風味", "Up to 4 items can be selected", "最多可选４项风味", "風味の選択は四つ以下です", "สามารถเลือกได้ถึง 4 รายการ"),
    MAX_COUNTRY_ITEM("產地只能選擇１個", "Can only choose one place of origin", "产地只能选择１个", "国は一つしかありません", "สามารถเลือกได้เพียงแห่งเดียวเท่านั้น"),
    CLOUD_SEARCH_TITLE("提醒您", "Remind you", "提醒您", "リマインド", "เตือนคุณ"),
    CLOUD_SEARCH_TITLE_MSG("請選擇項目後，再按確認", "Please select the item and press confirm", "请选择项目后，再按确认", "選択を選んで確認を押してください", "โปรดเลือกรายการและกดยืนยัน"),

    //-----咖啡雲 個人顯示＆個人資料編輯-----
    CLOUD_PERSONAL_TITLE("個人", "Personal", "个人", "個人", "ส่วนบุคคล"),
    CLOUD_PERSONAL_LOGOUT("登出帳號", "Logout", "注销账号", "ログアウト", "ล็อกเอ้าท์"),
    CLOUD_PERSONAL_EDIT_ACCOUNT("編輯帳號", "Edit account", "编辑帐号", "アカウントの編集", "ล็อกเอ้าท์"),
    CLOUD_PERSONAL_USER_NAME("使用者名稱", "User name", "使用者名称", "ユーザーネーム", "แก้ไขบัญชี"),
    CLOUD_PERSONAL_BIRTHDAY("生日", "Birthday", "生日", "生年月日", "วันเกิด"),
    CLOUD_PERSONAL_GENDER("性別", "Gender", "姓别", "性別", "เพศ"),
    CLOUD_PERSONAL_USER_DESCRIPTION("公開頁面簡介", "User description", "公开页面简介", "ユーザーの公開プロフィール", "คำอธิบาย"),
    CLOUD_PERSONAL_RESET_PASSWORD("重設密碼", "Reset Password", "重设密码", "パスワードをリセットする", "รีเซ็ตรหัสผ่าน"),

    OLD_PASSWORD("請輸入舊密碼", "type origin password", "请输入旧密码", "元パスワード入力", "โปรดป้อนรหัสผ่านเดิม"),
    NEW_PASSWORD("請輸入新密碼", "type new password", "请输入新密码", "新パスワード入力", "โปรดป้อนรหัสผ่านใหม่"),
    REINPUT_PASSWORD("請再次輸入密碼", "type confirm password", "请再次输入密码", "確認パスワード入力", "โปรดป้อนรหัสผ่านของคุณอีกครั้ง"),
    DIFF_PASSWORD("密碼不ㄧ致", "incorrect password", "密码不ㄧ致", "パスワードエラー", "รหัสผ่านไม่ถูกต้อง"),

    UPDATE_PASSWORD_SUCCESS("密碼修改成功", "password is already change", "密码修改成功", "パスワード変更完了", "การแก้ไขรหัสผ่านที่สำเร็จ"),
    UPDATE_PASSWORD_FAIL("密碼修改失敗", "password update fail", "密码修改失败", "パスワード変更エラー", "ไม่สามารถเปลี่ยนรหัสผ่าน"),
    UPDATE_INFO_SUCCESS("會員資料修改成功", "info is already change", "会员资料修改成功", "ユーザー情報変更完了", "ข้อมูลสมาชิกได้รับการแก้ไขเรียบร้อยแล้ว"),
    UPDATE_INFO_FAIL("會員資料修改失敗", "info update fail", "会员资料修改失败", "ユーザー情報変更エラー", "ไม่สามารถแก้ไขข้อมูลการเป็นสมาชิกได้"),
    UPDATE_PROFILE_PIC_SUCCESS("頭貼更新成功", "profile picture is already change", "头贴更新成功", "ユーザー写真変更完了", "การอัปเดตสำเร็จแล้ว"),
    UPDATE_PROFILE_PIC_FAIL("頭貼更新失敗", "update profile picture fail", "头贴更新失败", "ユーザー写真変更エラー", "การอัปเดตส่วนหัวล้มเหลว"),

    LOGOUT_TITLE("登出", "log out", "登出", "ログアウト", "ออกจากระบบ"),
    LOGOUT_MSG("確定登出?", "To confirm log out ?", "確定登出?", "ログアウトしますか？", "คุณแน่ใจหรือว่าต้องการออกจากระบบ?"),

    //-----咖啡雲 裝置-----
    DEVICE("裝置", "Device", "装置", "装置", "อุปกรณ์"),
    CUSTOMER_SERVICE("客服", "Customer Service", "客服", "カスタマーサービス", "บริการลูกค้า"),
    MANUAL("使用手冊", "Manual", "使用手册", "マニュアル", "คู่มือ"),
    UNSIGN("取消登記", "Unsign", "取消登记", "アカウントの削除", "ไม่ได้กำหนด"),

    //-----我的配方 列表-----
    NO_RECIPES("這裡現在沒有任何配方，來這邊開始你的第一筆吧", "No new recipe now start your first recipe", "这里现在没有任何配方，来这边开始你的第一笔吧", "新しいレシピがございません。新しいレシピを追加しましょう。", "ไม่มีสูตรที่นี่มาที่นี่และเริ่มต้นของคุณครั้งแรก"),
    SELECT_ADD_WAY("選擇新增方式", "SELECT", "选择新增方式", "新しいメソッドを選択", "เลือกวิธีการใหม่"),
    CREATE_RECIPE("新增空白配方", "Create Recipe", "新增空白配方", "レシピ追加", "สร้างเมนู"),
    EDIT_RECIPE("編輯參考配方", "Edit Recipe", "编辑参考配方", "レシピの編修", "แก้ไขเมนู"),

    //-----我的配方 詳細預覽-----
    ENTER_EDIT_PAGE("進入編輯頁", "Enter the edit page", "进入编辑页", "編集ページに入る", "ป้อนหน้าแก้ไข"),
    OVERWRITE_MACHINE("覆寫機器選項", "Overwrite machine options", "覆写机器选项", "マシンオプションを上書きする", "เขียนทับตัวเลือกเครื่อง"),
    RECIPE_OVERRIDE("覆寫中...", "overriding", "覆写中...", "保存中", "แทนที่"),

    //-----我的配方 新增配方＆編輯配方-----
    MY_EDIT_RECIPE("編輯配方", "Edit recipe", "编辑配方", "レシピの編修", "แก้ไขเมนู"),
    SAVE("儲存", "Save", "储存", "保存", "บันทึก"),
    RECIPE_NAME("配方名稱", "Recipe name", "配方名称", "レシピ名", "ชื่อเมนู"),
    TEMPERATURE("水溫", "Temperature", "水温", "温度", "อุณหภูมิ"),
    BREW_NOTE("沖煮筆記", "Brew note", "冲煮笔记", "抽出ノート", "บันทึกการชง"),

    FORMULA_NAME_CANT_EMPTY("配方名稱不可為空", "please type formula name", "配方名称不可为空", "レシピの名前を入力ください", "ชื่อสูตรต้องไม่ว่างเปล่า"),
    FORMULA_TEMP_CANT_EMPTY("水溫不可為空", "please select temperature", "水温不可为空", "水温度を入力ください", "อุณหภูมิของน้ำต้องไม่ว่างเปล่า"),
    FORMULA_BEAMGRAM_CANT_EMPTY("粉重不可為空", "please select bean gram", "粉重不可为空", "豆重を入力ください", "สีชมพูต้องไม่ว่างเปล่า"),
    FORMULA_STEPS_EMPTU("請至少新增一個沖泡步驟", "at least add one step", "请至少新增一个冲泡步骤", "レシピの階段を追加してください", "โปรดเพิ่มขั้นตอนการผลิตอย่างน้อยหนึ่งขั้นตอน"),
    FORMULA_STEPS_KEEP_BLOOM("請至少保留悶蒸的步驟", "at least add bloom step", "请至少保留闷蒸的步骤", "レシピの蒸らしを追加してください", "โปรดเก็บขั้นตอนการนึ่งไว้อย่างน้อยที่สุด"),
    FORMULA_BEAMDEGREE_CANT_EMPTY("研磨度不可為空", "please select bean degree", "研磨度不可为空", "豆粉の細かさを入力ください", "องศาการเจียรไม่ต้องว่างเปล่า"),
    FORMULA_ALREADY_DELETE("配方已刪除", "is delete", "配方已删除", "レシピはもう削除されました", "สูตรถูกลบแล้ว"),
    RECIPE_ADD_NOW("新增配方中...", "add recipe immediately", "新增配方中...", "新しいレシピを作ります", "สูตรใหม่"),
    RECIPE_UPDATE_NOW("修改配方中...", "update recipe immediately", "修改配方中...", "レシピ変更中", "แก้ไขสูตร"),
    RECIPE_DELETE_NOW("配方刪除中...", "delete recipe immediately", "配方刪除中...", "レシピ削除中", "การนำสูตรออก"),
    RECIPE_SAVE_NOW("儲存配方中...", "save recipe immediately", "储存配方中...", "レシピを保存する", "สูตรเก็บ"),
    RECIPE_DELETE("是否刪除該筆配方?", "delete recipe immediately", "是否删除该笔配方?", "このレシピを削除するか？", "คุณต้องการลบสูตรหรือไม่?"),
    RECIPE_DELETE_TITLE("刪除配方", "Delete Recipe", "刪除配方", "レシピを削除する", "ลบสูตร"),
    RECIPE_NOT_OWNER("不是擁有者無法修改、新增、刪除配方", "Not Recipe Owner", "不是拥有者无法修改、新增、删除配方", "レシピの所有者じゃありません", "เจ้าของไม่สามารถแก้ไขเพิ่มหรือลบสูตร"),
    RECIPE_CANCEL_BOOK("是否取消收藏該筆配方?", "cancel recipe bookmark?", "是否取消收藏该笔配方?", "レシピ収集をキャンセルするか？", "ยกเลิกการเก็บรวบรวมสูตรหรือไม่?"),

    FORMULA_TEMP("溫度", "temp", "溫度", "温度", "อุณหภูมิ"),
    RATIO_WITH_POWDER_WATER("粉水比", "Brew ratio", "粉水比", "抽出率", "อัตราส่วนผงต่อน้ำ"),
    WEIGHT_WATER("水重", "Water Volume", "水重", "水量", "น้ำหนักน้ำ"),

    ADD_SUCCESSED("新增成功", "successful", "新增成功", "新設完了", "เพิ่มความสำเร็จใหม่"),
    ADD_FAIL("新增失敗", "fail!!", "新增失敗", "新設に失敗しました", "ใหม่ล้มเหลว"),
    UPDATE_SUCCESSED("更新成功", "Update Successful", "更新成功", "変更完了", "การอัปเดตที่ประสบความสำเร็จ"),
    UPDATE_FAIL("更新失敗", "Update Fail", "更新失败", "変更に失敗しました", "ไม่สามารถอัปเดต"),

    //-----我的配方 沖煮筆記-----
    FORMULA_DRIPPER("沖煮器材", "dripper", "沖煮器材", "ドリッパー", "อุปกรณ์ต้มเบียร์"),
    FORMULA_GRINDER("磨豆器材", "grinder", "磨豆器材", "グラインダー", "บดอุปกรณ์ถั่ว"),
    FORMULA_PROCESS("處理法", "process", "处理法", "精製法", "วิธีการประมวลผล"),
    FORMULA_ROAST("烘培", "roast", "烘培", "焙煎", "การอบ"),
    FORMULA_ACIDITY("酸度", "acidity", "酸度", "酸味", "ความเป็นกรด"),
    FORMULA_SWEET("甜度", "sweet", "甜度", "甘さ", "ความหวาน"),
    FORMULA_AROMA("醇厚", "aroma", "醇厚", "アロマ", "กลมกล่อม"),
    FORMULA_BODY("香氣", "body", "香气", "ボディー", "กลิ่นหอม"),
    FORMULA_AFTERTASTE("餘韻", "aftertaste", "余韵", "アフターテイスト", "เสร็จสิ้น"),

    NOTE("筆記:", "Note:", "笔记:", "ノート:", "โน๊ต"),

    //-----機器配方 未連線-----
    MACHINE_RECIPE("機器配方", "Machine Recipe", "机器配方", "装置レシピ", "เมนูเครื่อง"),
    SAMANTHA_PAGE_NO_CONNECTION("未連線", "No connection", "未联机", "接続しておりません。", "ไม่สามารถติดต่อได้"),
    SAMANTHA_PAGE_NOT_CONNECTED_TITLE("啟動 SAMANTHA 電源之後，點下方的「尚未連線」以進行連線。", "Turn on SAMANTHA and click below button 'NO MACHINE' to connected", "启动 SAMANTHA 电源之后，点下方的「尚未联机」以进行联机。", "Samanthaの電源をOnにしてから、下記の「接続されてません」をクリックしてオンライン接続してください。", "กด samantha และ ปุ่ม No Machine เพื่อเริ่มใช้งาน"),

    DEVICE_LOST_CONNECT("偵測到裝置尚未連線，是否前往連線?", "connection lost", "侦测到装置尚未连线，是否前往连线?", "接続が切れました", "ตรวจพบว่าอุปกรณ์ไม่เชื่อมต่ออยู่หรือไม่?"),

    //-----達人手沖 準備沖泡＆加熱階段-----,
    ClEAR("清空", "Clear", "清空", "空", "ชัดเจน"),
    READY_BREW("準備沖泡", "Ready to brew", "准备冲泡", "醸造の準備", "ชัดเจน"),
    SET_TEMPERATURE("設定水溫", "Set water temperature", "设定水温", "水温を設定する", "ตั้งอุณหภูมิของน้ำ"),
    HEATING("加熱中..", "Heating..", "加热中", "暖房", "ความร้อน"),

    //-----達人手沖 Toast msg-----
    START_HEATING("開始加熱", "Start heating", "开始加热", "加熱を開始する", "เริ่มทำความร้อน"),
    STEAMING("悶蒸中", "Steaming", "闷蒸中", "蒸す", "ยับยั้งใน"),
    FIRST_WATER("第一次出水", "First out of water", "第一次出水", "最初の水から", "ออกจากน้ำครั้งแรก"),
    WATER_NOT_ENOUGH("水不夠，請至少加入250ml的水", "Water is not enough, please add at least 250ml of water", "水不够，请至少加入250ml的水", "水は十分ではありません、少なくとも250mlの水を加えてください", "น้ำไม่เพียงพอโปรดเพิ่มน้ำอย่างน้อย 250 มิลลิลิตร"),
    GREATER_TEMPERATURE("目標溫度必須大於現在溫度", "The target temperature must be greater than the current temperature", "目标温度必须大于现在温度", "目標温度は現在の温度より大きくなければなりません", "อุณหภูมิเป้าหมายต้องมากกว่าอุณหภูมิปัจจุบัน"),
    MACHINE_ERROR("機器狀態錯誤", "Machine status error", "闷蒸中", "マシン状態エラー", "ข้อผิดพลาดสถานะเครื่อง"),
    MACHINE_ERROR_BEING_BREW("機器狀態錯誤，目前正在沖煮中", "The machine is in the wrong state and is currently being brewed", "机器状态错误，目前正在冲煮中", "機械が間違った状態にあり、現在醸造中です。", "เครื่องอยู่ในสถานะที่ไม่ถูกต้องและกำลังมีการชงอยู่"),
    MACHINE_ERROR_CURRENTLY("機器狀態錯誤，目前正在排水中", "The machine is in the wrong status and is currently draining", "机器状态错误，目前正在排水中", "マシンのステータスが間違っていて現在排水中です", "เครื่องอยู่ในสถานะที่ไม่ถูกต้องและกำลังระบายน้ำอยู่"),
    MACHINE_ERROR_CLEAN("機器狀態錯誤，目前正在清潔中", "The machine is in an error state and is currently cleaning", "机器状态错误，目前正在清洁中", "マシンはエラー状態にあり、現在クリーニング中です。", "เครื่องกำลังอยู่ในสถานะผิดพลาดและกำลังทำความสะอาดอยู่"),
    MACHINE_ERROR_FILLING_WATER("機器狀態錯誤，機器正在達人手沖注水狀態", "The machine is in the wrong state and the machine is filling the water", "机器状态错误，机器正在达人手冲注水状态", "機械が間違った状態にあり、機械が水を満たしている", "เครื่องอยู่ในสถานะที่ไม่ถูกต้องและเครื่องกำลังเติมน้ำ"),
    STOP_HEATING("停止加熱", "Stop heating", "停止加热", "加熱を止める", "หยุดความร้อน"),
    NO_MACHINE("沒有連線咖啡機", "No connection coffee machine", "没有连线咖啡机", "接続コーヒーマシンなし", "ไม่มีเครื่องชงกาแฟเชื่อมต่อ"),
    CLOUD_PAGE_NO_MACHINE("尚未連線", "no machine", "尚未联机", "サマンサが検出できません。", "ไม่มีเครื่อง"),
    LOW_VOLUME("水量不足", "Low volume", "水量不足", "不十分な水", "น้ำไม่เพียงพอ"),
    OVERWRITE_COMPLETED("覆寫完成", "Overwrite completed", "覆写完成", "上書きが完了しました", "เขียนทับเสร็จแล้ว"),
    MACHINE_RECIPE_NOT_READ("機器配方未讀取", "Machine recipe not read", "机器配方未读取", "マシンレシピが読み込まれない", "สูตรเครื่องไม่อ่าน"),
    START_TO_BREW("沖煮啟動中", "Start to brew", "冲煮启动中", "沸騰開始", "เริ่มต้นเดือด"),

    //-----達人手沖 悶蒸＆出水階段-----
    BARISTA_MODE("達人手沖", "Barista Mode", "达人手冲", "バリスターモード", "โหมดบาริสต้า"),
    BLOOM("悶蒸", "Bloom", "悶蒸", "蒸らし", ""),
    SET_FLOW_RATE("設定流速", "Set flow rate", "设定流速", "フローレートの設定", "ตั้งอัตราการไหล"),

    //-----達人手沖 完成沖泡-----
    FINISHED("完成沖泡!", "Complete brewing!", "完成冲泡", "醸造を完了する", "สำเร็จ"),

    //-----機器配方 請稍後-----
    PLEASE_WAIT("請稍後", "Please wait", "请稍后", "お待ちください", "โปรดรอสักครู่"),

    //-----機器配方 萃取中-----
    EXTRACTION("萃取中..", "Extraction..", "萃取中", "抽出", "การสกัด"),

    //----------我的裝置-------------
    DEVICE_PRODUCT_TITLE("產品", "Product", "产品", "製品", "สินค้า"),
    DEVICE_PRODUCT_ID("序號", "ID", "序号", "いいえ", "เลขที่"),
    DEVICE_PRODUCT_REGISTER_TIME("註冊時間", "Date Of Registration", "注册时间", "登録時間", "เวลาลงทะเบียน"),
    DEVICE_SERVICE_BUTTON("客服", "Service", "客服", "カスタマーサービス", "บริการลูกค้า"),
    DEVICE_MANUAL_BUTTON("使用手冊", "Manual", "使用手册", "マニュアル", "คู่มือ"),
    DEVICE_CANCEL_BUTTON("取消登記", "Cancel", "取消登记", "登録をキャンセルする", "ยกเลิกการลงทะเบียน"),

    //----- 清潔模式 page 1-----
    MACHINE_USAGE("機器使用量", "Machine usage", "机器使用量", "マシン使用量", "การใช้เครื่อง"),
    MACHINE_AND_COFFEE_QUALITY("為維持機器與咖啡品質, 使用達50000ml建議進行清潔模式", "To maintain machine and coffee quality, use up to 50000ml recommended cleaning mode", "为维持机器与咖啡品质, 使用达50000ml建议进行清洁模式", "マシンとコーヒーの品質を維持するには、50000mlまでのクリーニングモードを推奨します", "เพื่อรักษาคุณภาพเครื่องและกาแฟให้ใช้โหมดการทำความสะอาดที่แนะนำถึง 50000 มล"),
    START_CLEANING("開始清潔", "Start cleaning", "开始清洁", "クリーニングを開始する", "เริ่มทำความสะอาด"),

    //----- 清潔模式 page 2-----
    READY("準備", "Ready", "准备", "準備する", "พร้อมแล้ว"),
    CITRIC_ACID_AND_WATER_MIXED_IN_CONTAINER("在容器內將30g的檸檬酸(一湯匙)及1300g的水混合均勻後倒入水箱中", "30g of citric acid (1 tablespoon) and 1300g of water are mixed in the container and poured into the water tank.", "在容器内将30g的柠檬酸(一汤匙)及1300g的水混合均匀后倒入水箱中", "クエン酸（大さじ1杯）30gおよび水1300gを容器内で混合し、水槽に注ぐ。", "30g ของกรดซิตริก (1 ช้อนโต๊ะ) และ 1300g ของน้ำผสมในภาชนะและเทลงในถังน้ำ"),
    CITRIC_ACID("檸檬酸30g", "Citric acid 30g", "柠檬酸30g", "クエン酸30g", "กรดซิตริก 30g"),
    WATER_ML("水1300ml", "Water 1300ml", "水1300ml", "水1300ml", "น้ำ 1300ml"),
    NEXT("下一步", "The next step", "下一步", "次のステップ", "ขั้นตอนต่อไป"),

    //----- 清潔模式 page 3-----
    CITRIC_ACID_CLEANING("檸檬酸清洗", "Citric acid cleaning", "柠檬酸清洗", "クエン酸洗浄", "ทำความสะอาดกรดซิตริก"),
    RECEIVING_CONTAINER_PLACED_PRESSED_CLEANING("在出水口下方放置好承接容器後按下\"清洗\"按鈕即開始加熱清洗", "Citric acid cleaning", "在出水口下方放置好承接容器后按下\"清洗\"按钮即开始加热清洗", "コンテナをコンセントの下に置き、加熱とクリーニングを開始するために\"Clean\"ボタンを押します", "วางภาชนะรับที่อยู่ใต้เต้าเสียบและกดปุ่ม \"Clean\" เพื่อเริ่มทำความร้อนและทำความสะอาด"),
    CITRIC_ACID_WATER_HEATING("水箱內的檸檬酸水將會被加熱至96", "The citric acid water in the tank will be heated to 96", "水箱内的柠檬酸水将会被加热至96", "タンク内のクエン酸水は96℃に加熱されます", "น้ำกรดซิตริกในถังจะถูกให้ความร้อนถึง 96"),
    CLEAN("清洗", "Clean", "清洗", "クリーニング", "ล้าง"),

    //----- 清潔模式 page 4-----
    CLEAR_WATER_CLEANING("清水清洗", "Clear water cleaning", "清水清洗", "清潔な水の清掃", "ล้างน้ำให้สะอาด"),
    PLACE_THE_CONTAINER_AT_THE_OUTLET_AND_PRESS_CLEAN("水箱內加入的水,確認出水口下放置好承接容器後按下\"清洗\"", "Citric acid cleaning", "水箱内加入的水,确认出水口下放置好承接容器后按下\"清洗\"", "水タンクに水を満たし、受け入れコンテナが水出口の下に置かれていることを確認して、\"クリーン\"を押してください。", "เติมน้ำในถังน้ำให้ยืนยันว่าภาชนะบรรจุที่ได้รับจะอยู่ใต้เต้าเสียบน้ำและกด \"Clean\""),
    WATER_HEATING_AFTER_WATER("水箱內的水將會被加熱至96後出水", "The water in the tank will be heated to 96 after the water", "水箱内的水将会被加热至96后出水", "タンクの水は水の後に96℃に加熱されます", "น้ำในถังจะถูกให้ความร้อนถึง 96 หลังจากที่น้ำ"),

    //----- 清潔模式 加熱階段-----
    CLEANING_HEATING("清洗加熱..", "Cleaning heating..", "清洗加热..", "クリーニング加熱..", "ทำความสะอาดเครื่องทำความร้อน"),

    //----- 清潔模式 出水階段-----
    CLEAN_WATER("清洗出水..", "Clean water..", "清洗出水..", "きれいな水..", "น้ำสะอาด"),

    //----- 清潔模式 完成-----
    CLEANING_COMPLETED("清洗完成", "Cleaning completed", "清洗完成", "クリーニングが完了しました", "การทำความสะอาดเสร็จสิ้น"),

    //=====================================================================================
    //=end=
    //=====================================================================================
    ;

    //-----------------------------------------------
    // global member
    //-----------------------------------------------
    private String m_Eg; // 英文
    private String m_Ch; // 繁中
    private String m_Cn; // 簡中
    private String m_Jp; // 日文
    private String m_Th; // 泰文

    public static final String ENGLISH = "en-us";
    public static final String CHINESE = "zh-tw";
    public static final String SIMCHINESE = "zh-cn";
    public static final String JAPANESE = "ja-jp";
    public static final String KINGDOM_ENGLISH = "en-gb";
    public static final String THAI = "th-th";

    private Lst<String> m_options = Lst.of(ENGLISH, CHINESE, SIMCHINESE, JAPANESE);

    //-----------------------------------------------
    // construct
    //-----------------------------------------------
    MutiMsg(String ch, String eg, String cn, String jp, String th) {
        m_Ch = ch;
        m_Eg = eg;
        m_Cn = cn;
        m_Jp = jp;
        m_Th = th;
    }

    //-----------------------------------------------
    // public method
    //-----------------------------------------------
    public String msg() {

        Locale locale = Locale.getDefault();
        String language = locale.getLanguage() + "-" + locale.getCountry();
        String currLanguage = SpCurrentLanguage.getOr(language.toLowerCase());

        CsLog.d(MutiMsg.class, " current language " + currLanguage.toLowerCase());
        return Lst.of(m_Eg, m_Ch, m_Cn, m_Jp)
                .getOr(m_options.find(currLanguage),  m_Ch); // default language

    }
}


//--------------------------------------------------------------------------------------------------
// checker
//--------------------------------------------------------------------------------------------------
class Checker { // 用以 check 哪個為空

    static final String ch = "中文";
    static final String eg = "英文";
    static final String jp = "日文";
    static final String s_ch = "簡中";
    static final String thai = "泰文";

    public static void main(String[] args) {
//        checkMutiMsg();
        checkHasEmpty();
    }

    private static void checkHasEmpty() {
        FileUtil fileUtil = new FileUtil();
        Rows rows = fileUtil.mutiMembers(Pub.BRUCE_MUTIMSG_PAGE_PATH);
        rows.toLst().forEach(new Lst.IConsumer<Row>() {
            @Override
            public void runEach(int i, Row r) {
                if (r.childHTHasEmpty())
                    r.println();
            }
        });


    }

    public static void checkMutiMsg() {

        FileUtil fileUtil = new FileUtil();

        Rows rows = fileUtil.muti2Rows(Pub.BRUCE_MUTIMSG_PAGE_PATH);
        Lst.of(rows.toList()).forEach(new Lst.IConsumer<Row>() {
            @Override
            public void runEach(int i, Row r) {
                Lst.of(r).forEach(new Lst.IConsumer<String>() {
                    @Override
                    public void runEach(int j, String s) {
                        if (s.isEmpty())
                            JavaTools.println(Lst.of(ch, eg, s_ch, jp, thai).get(j) + " 是空的");
                    }
                });
            }
        });
    }

}
