package com.library.android_common.file;

import com.library.android_common.component.common.Lst;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Created by Ray on 17/06/2018.
 */

public class Rows extends ArrayList<Row> {

    private ArrayList<Row> m_self;

    //----------------------------------------------
    // Construct
    //----------------------------------------------
    public Rows(Row... rows) {
        m_self = this;
        for (Row r : rows) {
            if (r.size() != 0)
                m_self.add(r);
        }
    }

    public Rows(ArrayList<Row> rows) {
        m_self = rows;
    }

    static public Rows of(Row... rows) {
        return new Rows(rows);
    }

    static public Rows of(ArrayList<Row> rows) {
        return new Rows(rows);
    }

    static public Rows ofMuti(ArrayList<String> msgs) {
        Rows rows = new Rows();
        for (String s : msgs)
            rows.add(Row.of(StrUtil.mutiMsg2List(s)));
        return rows;
    }

    static public Rows ofMutiContent(ArrayList<String> contents) { // 取 MutiMsg.class 裡面的 content
        Rows rows = new Rows();
        for (String s : contents)
            rows.add(Row.of(StrUtil.mutiMsgDecode(s)));
        return rows;
    }


    //----------------------------------------------
    // Public Method
    //----------------------------------------------
    public Row getRow(int position) {
        return m_self.get(position);
    }

    public Rows sub(int fromIndex) {
        return sub(fromIndex, m_self.size());
    }

    public Rows sub(int fromIndex, int toIndex) {
        final Rows rows = new Rows();
        Lst.of(m_self).sub(fromIndex, toIndex).forEach(new Lst.IConsumer<Row>() {
            @Override
            public void runEach(int i, Row r) {
                rows.add(r);
            }
        });
        return rows;
    }

    public Rows pick(int... pos) {
        Rows rows = new Rows();
        for (int na : pos)
            rows.add(m_self.get(na));
        return rows;
    }

    public ArrayList<Row> toList() {
        return m_self;
    }

    public Lst<Row> toLst() {
        return Lst.of(m_self);
    }

    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println_d(final String filter) {
        Lst.of(m_self).forEach(new Lst.IConsumer<Row>() {
            @Override
            public void runEach(int i, Row row) {
                row.println_d(filter);
            }
        });
    }

    public void println() {
        Lst.of(m_self).forEach(new Lst.IConsumer<Row>() {
            @Override
            public void runEach(int i, Row r) {
                JavaTools.println(r.toString());
            }
        });
    }


    //-----------------------------------------------
    // Test
    //-----------------------------------------------
    public static void main(String[] args) {

        Rows rows = Rows.of(Row.of("1", "2", "3"), Row.of("4", "5", "6"), Row.of("7", "8", "9"), Row.of("11", "12", "13"), Row.of("11", "12", "15"));
//        Rows.of(rows.toLst().toList()).

//        rows.pick(0, 2, 4).println();
//        rows.sub(1).println();
//        rows.getRow(2).println();

    }

}
