package com.library.android_common.file;

/**
 * Created by Ray on 17/06/2018.
 */

public class Pub {

    //-----------------------------------------------------
    // Content
    //-----------------------------------------------------
    public static final String RAYMOND_MUTIMSG_PAGE_PATH = "/Users/hiroia_20180511/Desktop/Raymond/Android/samantha/samantha-android/app/src/main/java/com/hiroia/samantha/enums/MutiMsg.java";
    public static final String BRUCE_MUTIMSG_PAGE_PATH = "/Users/hiroia-android/Desktop/bruce/android/samantha-android/app/src/main/java/com/hiroia/samantha/enums/MutiMsg.java";

    public static final String LOCAL_SERVICE_CHINESE = "file:///android_asset/service_ch.html";
    public static final String LOCAL_SERVICE_ENGLISH = "file:///android_asset/service_en.html";
    public static final String LOCAL_PRIVACY_CHINESE = "file:///android_asset/privacy_ch.html";
    public static final String LOCAL_PRIVACY_ENGLISH = "file:///android_asset/privacy_en.html";


    //-----------------------------------------------------
    // Test
    //-----------------------------------------------------
    public static void main(String[] args) {
//        JavaTools.println(desktop());
    }


}



