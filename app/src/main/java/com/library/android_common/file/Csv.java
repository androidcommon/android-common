package com.library.android_common.file;

/**
 * Create By Ray on 2018/6/15
 * To handel Csv file
 */
public class Csv {

    private Csv m_self;
    private Rows m_rows;

    //---------------------------------------------------
    // Construct
    //---------------------------------------------------
    public Csv() {
        m_self = this;
        m_rows = new Rows();
    }

    public Rows toRows() {
        return m_rows;
    }

    //---------------------------------------------------
    // Test
    //---------------------------------------------------
    public static void main(String[] args) {

    }

}
