package com.library.android_common.file;

import com.library.android_common.constant.CsLog;
import com.library.android_common.util.JavaTools;
import com.library.android_common.util.StrUtil;

import java.util.ArrayList;

/**
 * Created by Ray on 17/06/2018.
 * 用以表示 Csv 的 row
 */

public class Row extends ArrayList<String> {

    private ArrayList<String> m_self;

    //-----------------------------------------------------
    // Construct
    //-----------------------------------------------------
    public Row(ArrayList<String> as) {
        init();
        m_self = as;
    }

    public Row(String... sa) {
        init();
        for (String item : sa)
            m_self.add(item);
    }

    static public Row of(String... sa) {
        return new Row(sa);
    }

    static public Row of(String s) {
        return new Row(s.split(StrUtil.COMMA));
    }

    static public Row of(ArrayList<String> as) {
        return new Row(as);
    }

    private void init() {
        m_self = new ArrayList<>();
    }

    //-----------------------------------------------------
    // Public Method
    //-----------------------------------------------------
    public String toString() {
        return StrUtil.bracketsOf(StrUtil.builder(StrUtil.COMMA, m_self));
    }

    public String getCell(int position) {
        return m_self.get(position);
    }

    public boolean childHasEmpty() {
        for (String s : m_self)
            if (s.contains(StrUtil.SPACE))
                return true;
        return false;
    }

    public boolean childHTHasEmpty() { // HT : Header and Tail

        for (String s : m_self) {
            s = s.replaceAll(StrUtil.QUOTE, StrUtil.EMPTY);
            if (s.startsWith(StrUtil.SPACE) || s.endsWith(StrUtil.SPACE))
                return true;
        }
        return false;
    }

    //------------------------------------------------------
    // println
    public void println_d() {
        println_d(StrUtil.EMPTY);
    }

    public void println() {
        JavaTools.println(toString());
    }

    public void println_d(String filter) {
        CsLog.d(Row.class, filter.concat(toString()));
    }

    public ArrayList<String> toList() {
        return m_self;
    }
}
