package com.library.android_common.file;


import com.library.android_common.component.common.Lst;
import com.library.android_common.component.common.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by Ray on 17/06/2018.
 * 用以處理各種 file action
 */

public class FileUtil {

    private FileUtil m_self;
    private FileReader m_reader;
    private BufferedReader m_buffered;

    //------------------------------------------------
    // Construct
    //------------------------------------------------
    public FileUtil() {
        m_self = this;
    }


    //------------------------------------------------
    // Public Method
    //------------------------------------------------
    public FileUtil readFile(String path) {
        try {
            m_reader = new FileReader(path);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return m_self;
    }

    public ArrayList<String> toList() {
        ArrayList<String> rows = new ArrayList<>();
        try {
            m_buffered = new BufferedReader(m_reader);
            while (m_buffered.ready()) {
                rows.add(m_buffered.readLine());
            }
            m_reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rows;
    }

    public Rows toRows() {
        final Rows rows = new Rows();
        Lst.of(toList()).forEach(new Lst.IConsumer<String>() {
            @Override
            public void runEach(int i, String s) {
                rows.add(Row.of(s));
            }
        });
        return rows;
    }

    public void createFile(String path, String fileName) {
        File f = new File(path.concat(fileName));
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeInFile(final String path, ArrayList<String> rows) {
        try {
            final PrintWriter writer = new PrintWriter(path, "UTF-8");

            Lst.of(rows).forEach(new Lst.IConsumer<String>() {
                @Override
                public void runEach(int i, String s) {
                    writer.println(s);
                    writer.close();
                }
            });

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void rows2Muti(String path) {

    }

    public Rows mutiMembers(String path) {
        return Rows.ofMutiContent(mutifilter(path));
    }

    public Rows muti2Rows(String path) {
        return Rows.ofMuti(mutifilter(path));
    }


    //------------------------------------------------
    // Private Preference
    //------------------------------------------------
    private ArrayList<String> mutifilter(String path) { // 用以過濾 MutiMsg.class 裡面不是 member 的行數

        final Pair<Integer, Integer> pair = new Pair<>();
        final ArrayList<String> pureList = new ArrayList<>();
        Lst.of(readFile(path).toList()).forEach(new Lst.IConsumer<String>() {
            @Override
            public void runEach(int i, String s) {
                if (s.trim().contains("=start="))
                    pair.setK(i);

                if (s.trim().contains("=end="))
                    pair.setV(i);

                pureList.add(s);
            }
        });

        return Lst.of(pureList)
                .sub(pair.k() + 1, pair.v())
                .removeIf(new Lst.IPredicate<String>() {
                    @Override
                    public boolean remove(String s) {
                        return s.isEmpty() || s.trim().startsWith("//");
                    }
                })
                .toList();
    }


    //------------------------------------------------
    // Test
    //------------------------------------------------
    public static void main(String[] args) {

    }
}
