package com.library.android_common.util;

/**
 * Create By Ray on 2018/6/15
 */
public class JavaTools {

    static public void println(String content) {
        System.out.println(content);
    }

    static public String desktopPath() {
        return homePath() + "/" + "Desktop";
    }

    static public String homePath() {
        return System.getProperty("user.home");
    }


}
