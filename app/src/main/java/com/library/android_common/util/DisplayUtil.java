package com.library.android_common.util;

import android.content.Context;

/**
 * Create By Ray on 2018/6/29
 * To Convert px & dp & sp
 * reference address : https://www.cnblogs.com/duanweishi/p/4449588.html
 */
public class DisplayUtil {

    private static Context m_ctx;

    public static void init(Context ctx) {
        m_ctx = ctx;
    }

    public static int px2dip(float pxValue) {
        final float scale = m_ctx.getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int dip2px(float dipValue) {
        final float scale = m_ctx.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    public static int px2sp(float pxValue) {
        final float fontScale = m_ctx.getResources().getDisplayMetrics().scaledDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    public static int sp2px(float spValue) {
        final float fontScale = m_ctx.getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }
}
