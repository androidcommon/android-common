package com.library.android_common.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Create By Ray on 2018/7/3
 */
public class ToastUtil {

    static private Context m_ctx;

    //-----------------------------------------------------
    // init
    //-----------------------------------------------------
    static public void init(Context ctx) {
        m_ctx = ctx;
    }

    //-----------------------------------------------------
    // public method
    //-----------------------------------------------------
    static public void show(String content) {
        show(Toast.LENGTH_SHORT, content);
    }

    static public void show(int time, String content) {
        Toast.makeText(m_ctx, content, time).show();
    }

}
