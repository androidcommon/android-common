package com.library.android_common.util;

import com.library.android_common.component.TimeCounter;

/**
 * Create By Ray on 2018/6/6
 */
public class ThreadUtil {

    private static OnRunInNewThread sm_listener;
    private static OnRunThreadFinish sm_runThreadFinish;
    private static Thread m_thread;

    public static void createThread(OnRunInNewThread listener) {
        sm_listener = listener;
        init();
    }

    public static void createThread(OnRunThreadFinish listener) { //勿使用
        sm_runThreadFinish = listener;
        init();
    }

    //-----------------------------------------------------
    // Private Preference
    //-----------------------------------------------------
    static private void init() {
        //--------------------------------------
        m_thread = new Thread(new Runnable() {
            @Override
            public void run() {
                sm_listener.run();
            }
        });
        m_thread.start();
    }

    static private void threadFinishListener() { //勿使用
        if (!m_thread.isAlive()) {
            sm_runThreadFinish.finish();
        } else {
            TimeCounter.delay(1);
            threadFinishListener();
        }
    }

    //------------------------------------------------------
    // Interface
    //------------------------------------------------------
    public interface OnRunInNewThread {
        void run();
    }

    public interface OnRunThreadFinish {  //勿使用
        void run();

        void finish();
    }

}
