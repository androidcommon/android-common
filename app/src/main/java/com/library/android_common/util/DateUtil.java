package com.library.android_common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Raymond Zorro on 03/05/2018.
 * Date Utility
 */

public class DateUtil {

    //-----------------------------------------------------------------------
    // Now
    //-----------------------------------------------------------------------

    static public String now() {
        return now(DateFormat.yyyyMMdd_HH_mm.format());
    }

    static public String now(String sFormat) {

        Calendar calendar = Calendar.getInstance();
        Date Now = calendar.getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(sFormat);
        String NowString = simpleDateFormat.format(Now);

        return NowString;
    }

    static public String currentTime() {
        Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR);
        int min = c.get(Calendar.MINUTE);
        return hour + ":" + min;
    }


    //-----------------------------------------------------------------------
    // Date Format
    //-----------------------------------------------------------------------
    enum DateFormat {
        yyyyMMdd("yyyyMMdd"),
        yyyyMMdd_slash("yyyy/MM/dd"),
        yyyyMMdd_dash("yyyy-MM-dd"),
        yyyyMMdd_HH_mm("yyyyMMdd_HH:mm"),;
        private String m_sFormat;

        //----------------------------------------------------
        // construct
        //----------------------------------------------------

        DateFormat(String sFormat) {
            m_sFormat = sFormat;
        }

        //----------------------------------------------------
        // public method
        //----------------------------------------------------

        public String format() {
            return m_sFormat;
        }


    }


}
