package com.library.android_common.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Adjust by Ray 2018/07/09
 */

public class JSONUtil {

    //----------------------------------------------
    // String to JSONObject
    static public JSONObject toJSObj(String sJSON) {

        JSONObject jsObj = null;
        try {
            jsObj = new JSONObject(sJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsObj;
    }

    //----------------------------------------------
    // String to JSONOArray
    static public JSONArray toJSArr(String sJSON) {

        JSONArray jsArr = null;
        try {
            jsArr = new JSONArray(sJSON);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsArr;
    }

    // TODO library common checkout
//    static public JSONArray stepToJson(List<FormulaStepModel> ms){
//
//        JSONArray jsArr = new JSONArray();
//        try {
//            for (FormulaStepModel m : ms){
//                JSONObject jsObj = new JSONObject();
//                jsObj.put("water", m.getWaterUsed());
//                jsObj.put("speed", m.getPourSpeed());
//                jsObj.put("time", m.getInterval());
//                jsArr.put(jsObj);
//            }
//        }catch (JSONException e){
//            e.printStackTrace();
//        }
//        return jsArr;
//    }


}
