package com.library.android_common.util;

import android.content.Context;
import android.support.annotation.ColorRes;

/**
 * Created by Raymond Zorro on 12/04/2018.
 */

public class ColorUtil {

    //--------------------------------------------------
    public static final String WHITE = "#FFFFFF";
    public static final String BLACK = "#000000";
    public static final String TRANSPARENT = "#80000000";
    public static final String FORMUL_LIST_COLOR = "#9b9b9b";
    public static final String FORMUL_LIST_BLUE_COLOR = "#93abb1";
    public static final String CLOUD_SEARCH_ITEM_BACKGROUND = "#8fa4a8";


    //--------------------------------------------------
    // get color from xml, R.color.yourcolor
    public static int valueColor(Context ctx, @ColorRes int colorCode) {
        return ctx.getResources().getColor(colorCode);
    }
}
