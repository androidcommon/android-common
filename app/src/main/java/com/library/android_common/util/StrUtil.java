package com.library.android_common.util;

import android.annotation.SuppressLint;

import com.library.android_common.component.common.Lst;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by roy on 2017/8/24.
 */

public class StrUtil {

    //------------------------------------------------------------------------------
    // Symbol
    public static final String EMPTY = "";
    public static final String COMMA = ",";
    public static final String PERIOD = ".";
    public static final String SEMICOLON = ";";
    public static final String COLON = ":";
    public static final String DASH = "-";
    public static final String EQUAL = "=";
    public static final String QUOTE = "\"";
    public static final String SINGLE_QUOTE = "'";
    public static final String UNDER_LINE = "_";
    public static final String AT = "@";
    public static final String SLASH = "/";
    public static final String DIVIDER = "------------------------------------------\n";
    public static final String DOUBLE_DIVIDER = "------------------------------------------------------------------------------------\n";
    public static final String LEFT_BRACKET = "[";
    public static final String RIGHT_BRACKET = "]";
    public static final String LEFT_S_BRACKET = "(";
    public static final String RIGHT_S_BRACKET = ")";
    public static final String WRAP = "\n";

    //-----------------------------------------------------
    // with space
    public static final String SPACE = " ";
    public static final String DOUBLE_SPACE = "  ";
    public static final String TRIPLE_SPACE = "   ";
    public static final String TAB_SAPCE = "    ";
    public static final String SPACE_PERIOD = " . ";
    public static final String SPACE_COMMA = " ,  ";
    public static final String SPACE_SEMICOLON = " ; ";
    public static final String SPACE_COLON = " : ";
    public static final String SPACE_DASH = " - ";
    public static final String SPACE_EQUAL = " = ";
    public static final String SPACE_QUOTE = " \" ";
    public static final String SPACE_SINGLE_QUOTE = "'";
    public static final String SPACE_UNDER_LINE = " _ ";
    public static final String SPACE_AT = " @ ";
    public static final String SPACE_SLASH = " / ";
    public static final String SPACE_LEFT_BRACKET = " [ ";
    public static final String SPACE_RIGHT_BRACKET = " ] ";

    //------------------------------------------------------------------------------
    //character
    public static final String UTF_8 = "UTF-8";
    public static final String STR_NULL = "null";
    public static final String DEGREE_C = "℃";
    public static final String ZERO = "0";
    public static final String DECIMAL_ZERO = "0.0";

    //------------------------------------------------------------------------------
    // public method
    //------------------------------------------------------------------------------
    public static ArrayList<String> mutiMsgDecode(String s) {
        s = s.substring(0, s.contains("//") ? s.lastIndexOf("//") : s.length() - 2).trim(); // remove , )
        final String[] sa = s.split("[(]");
        return Lst.of(sa[1].split(StrUtil.COMMA)).map(new Lst.IMap<String>() {
            @Override
            public String mapEach(int i, String s) {
                return s.substring(s.indexOf(StrUtil.QUOTE), s.lastIndexOf(StrUtil.QUOTE));
            }
        }).toList();
    }

    public static ArrayList<String> mutiMsg2List(String s) {
        s = s.substring(0, s.length() - 2).trim(); // remove , )
        final String[] sa = s.split("[(]");

        return Lst.of(sa[1]).map(new Lst.IMap<String>() {
            @Override
            public String mapEach(int i, String s) {
                return sa[0] + StrUtil.COMMA + Lst.of(s.split(StrUtil.COMMA)).map(new Lst.IMap<String>() {
                    @Override
                    public String mapEach(int i, String s) {
                        return StrUtil.removeSyms(s);
                    }
                }).toString(StrUtil.COMMA).trim();
            }
        }).toList();
    }

    public static String removeSyms(String content) { // remove symbol head and tail if is equals
        if (!content.substring(0, 1).equals(content.substring(content.length() - 1)))
            return content;
        return content.substring(1, content.length() - 1);
    }

    public static String buildMutiMsg(int pickPosToName, ArrayList<String> as) { // pick name , 拿其中一個當 name
        return buildMutiMsg(as.get(pickPosToName), Lst.of(as).remove(pickPosToName).toList());
    }

    public static String buildMutiMsg(String sName, ArrayList<String> as) { //將 list convert to MutiMsg.class 的格式
        as = Lst.of(as).map(new Lst.IMap<String>() {
            @Override
            public String mapEach(int i, String s) {
                return doubleQuoteOf(s);
            }
        }).toList();
        return sName.concat(sbracketsOf(builder(StrUtil.COMMA, as))).concat(StrUtil.COMMA);
    }

    public static String spaceDashOf(String content) {
        return SPACE_DASH + content + SPACE_DASH;
    }

    public static String spaceOf(String content) {
        return SPACE + content + SPACE;
    }

    public static <E> String builder(String concat, Collection<E> lst) {
        StringBuilder sb = new StringBuilder();
        for (E e : lst)
            sb.append(e + concat);
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    public static String[] arrayOf(String... item) {
        return item;
    }

    public static String doubleQuoteOf(String content) {
        return "\"" + content + "\" ";
    }

    public static String singleQuoteOf(String content) {
        return "\'".concat(content).concat("\'");
    }

    public static String bracketsOf(String content) {
        return LEFT_BRACKET + content + RIGHT_BRACKET;
    }

    public static String sbracketsOf(String content) {
        return LEFT_S_BRACKET + content + RIGHT_S_BRACKET;
    }

    public static <E> String of(E... e) {
        return of(EMPTY, e);
    }

    public static <E> String of(String linker, E... e) {

        StringBuilder sb = new StringBuilder();
        for (Object o : e) {
            sb.append(o.toString().concat(linker));
        }
        if (!StrUtil.EMPTY.equals(linker))
            sb.setLength(sb.length() - 1);

        return sb.toString();
    }

    public static String str2Hex(String sContent) {
        return bytesToHex(sContent.getBytes());
    }

    public static String bytesToHex(byte[] in) {
        final StringBuilder builder = new StringBuilder();
        for (byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    //------------------------------------------------------------------------------
    // TODO 原作者-CU code 看是否還要留 不留即可砍
    //------------------------------------------------------------------------------
    public static byte[] hexStringToByteArray(String s) {
        s = s.replaceAll(COLON, EMPTY);
        s = s.toLowerCase();
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static int byteArrayToInt(byte[] b) {
        return b[3] & 0xFF |
                (b[2] & 0xFF) << 8 |
                (b[1] & 0xFF) << 16 |
                (b[0] & 0xFF) << 24;
    }

    public static String convertTimestampToLabel(long timestamp) {

        long second = timestamp / 1000;
        long minute = second / 60;
        long remainSecond = second % 60;

        String sRemainSec = String.valueOf(remainSecond).length() == 1 ? "0" + remainSecond : remainSecond + "";
        String buf = String.valueOf(minute) + "'" + sRemainSec + "''";

        return buf;
    }


    @SuppressLint("DefaultLocale")
    public static String padLeftZeroToNumberString(int totalDigits, String numberString) {
        String format = "%0" + totalDigits + "d";
        try {
            return String.format(format, Integer.parseInt(numberString));
        } catch (NumberFormatException notNumber) {
            // not a number
            return String.format(format, 0);
        }
    }
}

