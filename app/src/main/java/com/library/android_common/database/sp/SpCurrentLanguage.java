package com.library.android_common.database.sp;

import android.content.Context;
import android.content.SharedPreferences;

import com.library.android_common.component.common.Opt;
import com.library.android_common.util.StrUtil;

/**
 * Create By Ray on 2018/6/22
 * 用以儲存目前的 language
 */
public class SpCurrentLanguage {


    //--------------------------------------------------
    // Global Member
    //--------------------------------------------------
    static private SharedPreferences m_sp;
    static private SharedPreferences.Editor m_editor;

    private static final String SP_CURRENT_LANGUAGE = "sp_current_language";
    private static final String CURRENT_LANGUAGE = "current_language_use";


    //--------------------------------------------------
    // Public Method
    //--------------------------------------------------
    static public void init(Context ctx) {
        m_sp = ctx.getSharedPreferences(SP_CURRENT_LANGUAGE, Context.MODE_PRIVATE);
        m_editor = m_sp.edit();
    }

    static public void put(String language) {
        m_editor.putString(CURRENT_LANGUAGE, language);
        m_editor.commit();
    }

    static public String get() {
        return m_sp.getString(CURRENT_LANGUAGE, StrUtil.EMPTY);
    }

    static public String getOr(String s) {
        return Opt.of(get()).getOr(s);
    }

    static public void clear() {
        put(StrUtil.EMPTY);
    }


}
