package com.library.android_common.database.sp;

import android.content.Context;

import com.library.android_common.BuildConfig;
import com.library.android_common.constant.CsLog;
import com.library.android_common.database.sp.SpCurrentLanguage;
import com.library.android_common.database.sp.SpVersionManager;
import com.library.android_common.util.StrUtil;

/**
 * Created by Raymond Zorro on 11/05/2018.
 * register all share preference db
 */

public class SharePreferenceManager {

    //-----------------------------------
    // init all
    static public void init(Context ctx) {
        SpVersionManager.init(ctx);
//        SpMyFormula.init(ctx); // 留一個當範例參考
        SpCurrentLanguage.init(ctx);
    }

    //-----------------------------------
    // clear all
    static public void clearAll() {
//        SpMyFormula.clear(); // 留一個當範例參考
    }

    //-----------------------------------
    // print all
    static public void printAll_d(String filter) {
//        CsLog.d(SharePreferenceManager.class, filter.concat(" - SpBarista - 溫度 : " + SpBarista.getTemp())); // 留一個當範例參考
        CsLog.d(SharePreferenceManager.class, filter.concat(StrUtil.DIVIDER));
    }

    static public void syncSpVersion() {
        if (SpVersionManager.get() != BuildConfig.VERSION_CODE)
            clearAll(); // 如果版本不一致，清空所有SharePreference 暫存

        SpVersionManager.upgrade(); // 升級版本
    }


}
