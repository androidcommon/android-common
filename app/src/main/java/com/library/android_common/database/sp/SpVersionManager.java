package com.library.android_common.database.sp;

import android.content.Context;
import android.content.SharedPreferences;

import com.library.android_common.BuildConfig;

/**
 * Create By Ray on 2018/5/30
 * 用以管理所有 SharePreference 的 Version, 會 follow BuildConfig.Version
 * 管理版本用，所以不會有 clear()
 */
public class SpVersionManager {

    //--------------------------------------------------
    // Global Member
    //--------------------------------------------------
    static private SharedPreferences m_sp;
    static private SharedPreferences.Editor m_editor;

    private static final String SP_VERSION_MANAGER = "=SP_VERSION_MANAGER=";
    private static final String CONFIG_VERSION = "CONFIG_VERSION";


    //--------------------------------------------------
    // Public Method
    //--------------------------------------------------
    static public void init(Context ctx) {
        m_sp = ctx.getSharedPreferences(SP_VERSION_MANAGER, Context.MODE_PRIVATE);
        m_editor = m_sp.edit();
    }

    static public void upgrade() { // 根據 Build Version upgrade
        m_editor.putInt(CONFIG_VERSION, BuildConfig.VERSION_CODE);
        m_editor.commit();
    }

    static public int get() {
        return m_sp.getInt(CONFIG_VERSION, 0);
    }

}
